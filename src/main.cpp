/** 
	xr-video-player
	Copyright (C) 2022 dec05eba and yoshino

	This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <GL/glew.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>

#include <wayland-client.h>
#include <wayland-egl.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#define XR_USE_PLATFORM_EGL
#define XR_USE_GRAPHICS_API_OPENGL

#include <openxr/openxr.h>
#include <openxr/openxr_platform.h>
//#include "openxr_platform.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <pipewire/pipewire.h>
#include <glib.h>

#include <stdio.h>
#include <string>
#include <cstdlib>
#include <vector>

#include <unistd.h>
#include <signal.h>
#include <libgen.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include "screencast-portal.h"
#include "gl-egl-common.h"
#include "mpv.hpp"

#ifndef _countof
#define _countof(x) (sizeof(x)/sizeof((x)[0]))
#endif

#define HAND_COUNT 2


static bool g_bPrintf = true;


// true if XrResult is a success code, else print error message and return false
bool
xr_check(XrInstance instance, XrResult result, const char* format, ...)
{
	if (XR_SUCCEEDED(result))
		return true;

	char resultString[XR_MAX_RESULT_STRING_SIZE];
	xrResultToString(instance, result, resultString);

	char formatRes[XR_MAX_RESULT_STRING_SIZE + 1024];
	snprintf(formatRes, XR_MAX_RESULT_STRING_SIZE + 1023, "%s [%s] (%d)\n", format, resultString,
	         result);

	va_list args;
	va_start(args, format);
	vprintf(formatRes, args);
	va_end(args);

	return false;
}


// functions belonging to extensions must be loaded with xrGetInstanceProcAddr before use
static PFN_xrGetOpenGLGraphicsRequirementsKHR pfnGetOpenGLGraphicsRequirementsKHR = NULL;
static bool
load_extension_function_pointers(XrInstance instance)
{
	XrResult result =
	    xrGetInstanceProcAddr(instance, "xrGetOpenGLGraphicsRequirementsKHR",
	                          (PFN_xrVoidFunction*)&pfnGetOpenGLGraphicsRequirementsKHR);
	if (!xr_check(instance, result, "Failed to get OpenGL graphics requirements function!"))
		return false;

	return true;
}

// returns the preferred swapchain format if it is supported
// else:
// - if fallback is true, return the first supported format
// - if fallback is false, return -1
static int64_t
get_swapchain_format(XrInstance instance,
                     XrSession session,
                     int64_t preferred_format,
                     bool fallback)
{
	XrResult result;

	uint32_t swapchain_format_count;
	result = xrEnumerateSwapchainFormats(session, 0, &swapchain_format_count, NULL);
	if (!xr_check(instance, result, "Failed to get number of supported swapchain formats"))
		return -1;

	printf("Runtime supports %d swapchain formats\n", swapchain_format_count);
	int64_t* swapchain_formats = (int64_t*) malloc(sizeof(int64_t) * swapchain_format_count);
	result = xrEnumerateSwapchainFormats(session, swapchain_format_count, &swapchain_format_count,
	                                     swapchain_formats);
	if (!xr_check(instance, result, "Failed to enumerate swapchain formats"))
		return -1;

	int64_t chosen_format = fallback ? swapchain_formats[0] : -1;

	for (uint32_t i = 0; i < swapchain_format_count; i++) {
		printf("Supported GL format: %#lx\n", swapchain_formats[i]);
		if (swapchain_formats[i] == preferred_format) {
			chosen_format = swapchain_formats[i];
			printf("Using preferred swapchain format %#lx\n", chosen_format);
			break;
		}
	}
	if (fallback && chosen_format != preferred_format) {
		printf("Falling back to non preferred swapchain format %#lx\n", chosen_format);
	}

	free(swapchain_formats);

	return chosen_format;
}

typedef enum
{
	GRAPHICS_VULKAN,
	GRAPHICS_OPENGL,
	GRAPHICS_OPENGL_ES
} GraphicsAPI;

inline static void
XrMatrix4x4f_CreateProjectionFov(glm::mat4* result,
                                 GraphicsAPI graphicsApi,
                                 const XrFovf fov,
                                 const float nearZ,
                                 const float farZ)
{
	const float tanAngleLeft = tanf(fov.angleLeft);
	const float tanAngleRight = tanf(fov.angleRight);

	const float tanAngleDown = tanf(fov.angleDown);
	const float tanAngleUp = tanf(fov.angleUp);

	const float tanAngleWidth = tanAngleRight - tanAngleLeft;

	// Set to tanAngleDown - tanAngleUp for a clip space with positive Y
	// down (Vulkan). Set to tanAngleUp - tanAngleDown for a clip space with
	// positive Y up (OpenGL / D3D / Metal).
	const float tanAngleHeight =
	    graphicsApi == GRAPHICS_VULKAN ? (tanAngleDown - tanAngleUp) : (tanAngleUp - tanAngleDown);

	// Set to nearZ for a [-1,1] Z clip space (OpenGL / OpenGL ES).
	// Set to zero for a [0,1] Z clip space (Vulkan / D3D / Metal).
	const float offsetZ =
	    (graphicsApi == GRAPHICS_OPENGL || graphicsApi == GRAPHICS_OPENGL_ES) ? nearZ : 0;
	
	(*result)[0][0] = 2 / tanAngleWidth;
	(*result)[1][0] = 0;
	(*result)[2][0] = (tanAngleRight + tanAngleLeft) / tanAngleWidth;
	(*result)[3][0] = 0;

	(*result)[0][1] = 0;
	(*result)[1][1] = 2 / tanAngleHeight;
	(*result)[2][1] = (tanAngleUp + tanAngleDown) / tanAngleHeight;
	(*result)[3][1] = 0;

	if (farZ <= nearZ) {
		// place the far plane at infinity

		(*result)[0][2] = 0;
		(*result)[1][2] = 0;
		(*result)[2][2] = -1;
		(*result)[3][2] = -(nearZ + offsetZ);
	} else {
		// normal projection
		(*result)[0][2] = 0;
		(*result)[1][2] = 0;
		(*result)[2][2] = -(farZ + offsetZ) / (farZ - nearZ);
		(*result)[3][2] = -(farZ * (nearZ + offsetZ)) / (farZ - nearZ);
	}

	(*result)[0][3] = 0;
	(*result)[1][3] = 0;
	(*result)[2][3] = -1;
	(*result)[3][3] = 0;
}

enum class ContentMode {
	MPV,
	PIPEWIRE,
};

enum class ViewMode {
	LEFT_RIGHT,
	RIGHT_LEFT,
	PLANE,
	SPHERE360
};

enum class ProjectionMode {
	SPHERE,
	FLAT,
	CYLINDER, /* aka plane */
	SPHERE360
};

//-----------------------------------------------------------------------------
// Purpose:
//------------------------------------------------------------------------------
class CMainApplication
{
public:
	CMainApplication( int argc, char *argv[] );
	virtual ~CMainApplication();

	bool BInit();
	bool BInitGL();

	void Shutdown();

	int RunMainLoop();
	bool HandleInput();
    void zoom_in();
    void zoom_out();
	void RenderFrame();

	void ResetRotation();

	void SetupScene();
	void AddCubeToScene( const glm::mat4 &mat, std::vector<float> &vertdata );

	bool SetupStereoRenderTargets();
	void SetupCompanionWindow();

	void RenderCompanionWindow();
	void RenderScene( 
		int view_index,
		glm::mat4* projectionmatrix,
		glm::mat4* viewmatrix
	);

	GLuint CompileGLShader( const char *pchShaderName, const char *pchVertexShader, const char *pchFragmentShader );
	bool CreateAllShaders();

	int exit_code = 0;
private: 
	bool m_bDebugOpenGL;
	bool m_bVerbose;
	bool m_bPerf;
	bool m_bVblank;

	SDL_Window* desktop_window;
	SDL_GLContext gl_context;
	EGLSurface surface_draw;
	EGLSurface surface_read;

	// don't need a gl loader for just one function, just load it ourselves'
	PFNGLBLITNAMEDFRAMEBUFFERPROC _glBlitNamedFramebuffer;
	
	// we need an identity pose for creating spaces without offsets
	XrPosef identity_pose = {
		.orientation = {.x = 0, .y = 0, .z = 0, .w = 1.0},
		.position = {.x = 0, .y = 0, .z = 0}
		
	};
	
	// Changing to HANDHELD_DISPLAY or a future form factor may work, but has not been tested.
	XrFormFactor form_factor = XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY;

	// Changing the form_factor may require changing the view_type too.
	XrViewConfigurationType view_type = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;

	// Typically STAGE for room scale/standing, LOCAL for seated
	XrReferenceSpaceType play_space_type = XR_REFERENCE_SPACE_TYPE_LOCAL;
	XrSpace play_space = XR_NULL_HANDLE;

	// the instance handle can be thought of as the basic connection to the OpenXR runtime
	XrInstance instance = XR_NULL_HANDLE;
	// the system represents an (opaque) set of XR devices in use, managed by the runtime
	XrSystemId system_id = XR_NULL_SYSTEM_ID;
	// the session deals with the renderloop submitting frames to the runtime
	XrSession session = XR_NULL_HANDLE;

	// each graphics API requires the use of a specialized struct
	XrGraphicsBindingEGLMNDX graphics_binding_gl;
	struct gl_egl_data egl_data;

	// each physical Display/Eye is described by a view.
	// view_count usually depends on the form_factor / view_type.
	// dynamically allocating all view related structs instead of assuming 2
	// hopefully allows this app to scale easily to different view_counts.
	uint32_t view_count = 0;
	// the viewconfiguration views contain information like resolution about each view
	XrViewConfigurationView* viewconfig_views = NULL;

	// array of view_count containers for submitting swapchains with rendered VR frames
	XrCompositionLayerProjectionView* projection_views = NULL;
	// array of view_count views, filled by the runtime with current HMD display pose
	XrView* views = NULL;

	// array of view_count handles for swapchains.
	// it is possible to use imageRect to render all views to different areas of the
	// same texture, but in this example we use one swapchain per view
	XrSwapchain* swapchains = NULL;
	// array of view_count ints, storing the length of swapchains
	uint32_t* swapchain_lengths = NULL;
	// array of view_count array of swapchain_length containers holding an OpenGL texture
	// that is allocated by the runtime
	XrSwapchainImageOpenGLKHR** images = NULL;

	// depth swapchain equivalent to the VR color swapchains
	XrSwapchain* depth_swapchains = NULL;
	uint32_t* depth_swapchain_lengths = NULL;
	XrSwapchainImageOpenGLKHR** depth_images = NULL;

	// reuse this variable for all our OpenXR return codes
	XrResult result = XR_SUCCESS;
	
	XrPath hand_paths[HAND_COUNT];
	
	GLuint** framebuffers;

	struct
	{
		// supporting depth layers is *optional* for runtimes
		bool supported;
		XrCompositionLayerDepthInfoKHR* infos;
	} depth;

private: // SDL bookkeeping
	SDL_Window *m_pCompanionWindow;
	uint32_t m_nCompanionWindowWidth;
	uint32_t m_nCompanionWindowHeight;

	SDL_GLContext m_pContext;

private: // MPV
	const char *mpv_file = nullptr;
	Mpv mpv;
	std::thread mpv_thread;
	
private: // gl context mutex
	pthread_mutex_t context_mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
	int64_t context_mutex_counter = 0;

private: // OpenGL bookkeeping
	bool m_bResetRotation;
	bool m_bFreezRotation;

	float m_fScale;
	
	float m_fNearClip;
	float m_fFarClip;

	unsigned int m_uiVertcount;

	GLuint m_glSceneVertBuffer;
	GLuint m_unSceneVAO;
	GLuint m_unCompanionWindowVAO;
	GLuint m_glCompanionWindowIDVertBuffer;
	GLuint m_glCompanionWindowIDIndexBuffer;
	unsigned int m_uiCompanionWindowIndexSize;

	glm::mat4* m_mat4LastViewRotation;
	glm::mat4* m_mat4LastViewRotationReset;

	struct VertexDataScene
	{
		glm::vec3 position;
		glm::vec2 texCoord;
	};

	struct VertexDataWindow
	{
		glm::vec2 position;
		glm::vec2 texCoord;

		VertexDataWindow( const glm::vec2 & pos, const glm::vec2 tex ) :  position(pos), texCoord(tex) {	}
	};

	GLuint m_unSceneProgramID;
	GLuint m_unCompanionWindowProgramID;

	GLint m_nSceneMatrixLocation;
	GLint m_nSceneTextureOffsetXLocation;
	GLint m_nSceneTextureScaleXLocation;
	GLint m_nCursorLocation;
	GLint m_nArrowSizeLocation = -1;
	GLint m_myTextureLocation = -1;
	GLint m_arrowTextureLocation = -1;

	struct FramebufferDesc
	{
		GLuint m_nDepthBufferId;
		GLuint m_nRenderTextureId;
		GLuint m_nRenderFramebufferId;
		GLuint m_nResolveTextureId;
		GLuint m_nResolveFramebufferId;
	};
	FramebufferDesc leftEyeDesc;
	FramebufferDesc rightEyeDesc;
	
	void RenderFrameView(
		int view_index,
		XrSpaceLocation* hand_locations,
		glm::mat4 projectionmatrix,
		glm::mat4 viewmatrix,
		FramebufferDesc* desc
	);

	bool CreateFrameBuffer( int nWidth, int nHeight, FramebufferDesc &framebufferDesc );
	
	uint32_t m_nRenderWidth;
	uint32_t m_nRenderHeight;

private: // X compositor
	GMainLoop *loop;
	struct screencast_portal_capture *window_texture;
	bool follow_focused = false;
	bool focused_window_changed = true;
	bool focused_window_set = false;

	int mouse_x;
	int mouse_y;
	int window_width;
	int window_height;
	Uint32 window_resize_time;
	bool window_resized = false;
	bool got_texture = false;
	
    bool zoom_resize = false;

	GLint pixmap_texture_width = 0;
	GLint pixmap_texture_height = 0;

	ContentMode content_mode = ContentMode::PIPEWIRE;
	ProjectionMode projection_mode = ProjectionMode::SPHERE;
	double zoom = 0.0;
	float cursor_scale = 2.0f;
	ViewMode view_mode = ViewMode::LEFT_RIGHT;
	bool stretch = true;
	bool cursor_wrap = true;
	bool free_camera = false;
	bool use_system_mpv_config = false;

	GLuint arrow_image_texture_id = 0;
	int arrow_image_width = 1;
	int arrow_image_height = 1;
	int cursor_offset_x = 0;
	int cursor_offset_y = 0;

	float cursor_scale_uniform[2];
	double arrow_ratio;
	bool cursor_image_set = false;
};


//-----------------------------------------------------------------------------
// Purpose: Outputs a set of optional arguments to debugging output, using
//          the printf format setting specified in fmt*.
//-----------------------------------------------------------------------------
void dprintf( const char *fmt, ... )
{
	va_list args;
	char buffer[ 2048 ];

	va_start( args, fmt );
	vsnprintf( buffer, sizeof(buffer), fmt, args );
	va_end( args );

	if ( g_bPrintf )
		printf( "%s", buffer );
}

static void usage() {
	fprintf(stderr, "usage: xr-video-player [--sphere|--sphere360|--flat|--plane] [--left-right|--right-left] [--stretch|--no-stretch] [--zoom zoom-level] [--cursor-scale scale] [--cursor-wrap|--no-cursor-wrap] [--video video] [--use-system-mpv-config]\n");
    fprintf(stderr, "\n");
	fprintf(stderr, "OPTIONS\n");
    fprintf(stderr, "  --sphere          View the window as a stereoscopic 180 degrees screen (half sphere). The view will be attached to your head in vr. This is recommended for 180 degrees videos. This is the default value\n");
	fprintf(stderr, "  --sphere360       View the window as an equirectangular cube map. This is what is mostly used on youtube, where the video is split into top and bottom as a cubemap. The view will be attached to your head in vr\n");
	fprintf(stderr, "  --flat            View the window as a stereoscopic flat screen. This is recommended for stereoscopic videos and games\n");
    fprintf(stderr, "  --left-right      This option is used together with --flat, to specify if the left side of the window is meant to be viewed with the left eye and the right side is meant to be viewed by the right eye. This is the default value\n");
    fprintf(stderr, "  --right-left      This option is used together with --flat, to specify if the left side of the window is meant to be viewed with the right eye and the right side is meant to be viewed by the left eye\n");
    fprintf(stderr, "  --plane           View the window as a slightly curved screen. This is recommended for non-stereoscopic content\n");
    fprintf(stderr, "  --stretch         This option is used together with --flat, To specify if the size of both sides of the window should be combined and stretch to that size when viewed in vr. This is the default value\n");
    fprintf(stderr, "  --no-stretch      This option is used together with --flat, To specify if the size of one side of the window should be the size of the whole window when viewed in vr. This is the option you want if the window looks too wide\n");
    fprintf(stderr, "  --zoom            Change the distance to the window. This should be a positive value. In flat and plane modes, this is the distance to the window when the window is reset (with W key or controller trigger button). The default value is 0 for all modes except sphere mode, where the default value is 1. This value is unused for sphere360 mode\n");
    fprintf(stderr, "  --cursor-scale    Change the size of the cursor. This should be a positive value. If set to 0, then the cursor is hidden. The default value is 1 for all modes except sphere mode, where the default value is 0. The cursor is always hidden in sphere360 mode\n");
    fprintf(stderr, "  --cursor-wrap     If this option is set, then the cursor position in the vr view will wrap around when it reached the center of the window (i.e when it reaches the edge of one side of the stereoscopic view). This option is only valid for stereoscopic view (flat and sphere modes)\n");
    fprintf(stderr, "  --no-cursor-wrap  If this option is set, then the cursor position in the vr view will match the the real cursor position inside the window\n");
	fprintf(stderr, "  --free-camera     If this option is set, then the camera wont follow your position. This option is only applicable when not using --sphere or --sphere360\n");
	fprintf(stderr, "  --video <video>   Select the video to play (using mpv).\n");
	fprintf(stderr, "  --use-system-mpv-config   Use system (~/.config/mpv/mpv.conf) mpv config. Disabled by default\n");
    fprintf(stderr, "\n");
    fprintf(stderr, "EXAMPLES\n");
    fprintf(stderr, "  xr-video-player\n");
    fprintf(stderr, "  xr-video-player --flat\n");
    fprintf(stderr, "  xr-video-player --flat --right-left\n");
    fprintf(stderr, "  xr-video-player --plane --zoom 2.0\n");
	fprintf(stderr, "  xr-video-player --sphere --video $HOME/Videos/cool-vr-video.mp4\n");
	exit(1);
}

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CMainApplication::CMainApplication( int argc, char *argv[] )
	: m_pCompanionWindow(NULL)
	, gl_context(NULL)
	, m_nCompanionWindowWidth( 800 )
	, m_nCompanionWindowHeight( 600 )
	, m_unSceneProgramID( 0 )
	, m_unCompanionWindowProgramID( 0 )
	, m_bDebugOpenGL( false )
	, m_bVerbose( false )
	, m_bPerf( false )
	, m_bVblank( false )
	, m_unSceneVAO( 0 )
	, m_nSceneMatrixLocation( -1 )
	, m_nSceneTextureOffsetXLocation( -1 )
	, m_nSceneTextureScaleXLocation( -1 )
	, m_nCursorLocation( -1 )
	, m_bResetRotation( false )
{
	const char *projection_arg = nullptr;
	const char *view_mode_arg = nullptr;
	bool zoom_set = false;
	bool cursor_scale_set = false;
	bool cursor_wrap_set = false;

	for(int i = 1; i < argc; ++i) {
        if(strcmp(argv[i], "--sphere") == 0) {
			if(projection_arg) {
				fprintf(stderr, "Error: --sphere option can't be used together with the %s option\n", projection_arg);
				exit(1);
			}
			projection_mode = ProjectionMode::SPHERE;
			projection_arg = argv[i];
		} else if(strcmp(argv[i], "--flat") == 0) {
			if(projection_arg) {
				fprintf(stderr, "Error: --flat option can't be used together with the %s option\n", projection_arg);
				exit(1);
			}
			projection_mode = ProjectionMode::FLAT;
			projection_arg = argv[i];
		} else if(strcmp(argv[i], "--zoom") == 0 && i < argc - 1) {
			zoom = atof(argv[i + 1]);
			++i;
			zoom_set = true;
		} else if(strcmp(argv[i], "--cursor-scale") == 0 && i < argc - 1) {
			cursor_scale = atof(argv[i + 1]);
			++i;
			cursor_scale_set = true;
		} else if(strcmp(argv[i], "--left-right") == 0) {
			if(view_mode_arg) {
				fprintf(stderr, "Error: --left-right option can't be used together with the %s option\n", view_mode_arg);
				exit(1);
			}
			view_mode = ViewMode::LEFT_RIGHT;
			view_mode_arg = argv[i];
		} else if(strcmp(argv[i], "--right-left") == 0) {
			if(view_mode_arg) {
				fprintf(stderr, "Error: --right-left option can't be used together with the %s option\n", view_mode_arg);
				exit(1);
			}
			view_mode = ViewMode::RIGHT_LEFT;
			view_mode_arg = argv[i];
		} else if(strcmp(argv[i], "--plane") == 0) {
			if(projection_arg) {
				fprintf(stderr, "Error: --plane option can't be used together with the %s option\n", projection_arg);
				exit(1);
			}
			if(view_mode_arg) {
				fprintf(stderr, "Error: --plane option can't be used together with the %s option\n", view_mode_arg);
				exit(1);
			}
			view_mode = ViewMode::PLANE;
			projection_mode = ProjectionMode::CYLINDER;
			projection_arg = argv[i];
			view_mode_arg = argv[i];
		} else if(strcmp(argv[i], "--sphere360") == 0) {
			if(projection_arg) {
				fprintf(stderr, "Error: --sphere360 option can't be used together with the %s option\n", projection_arg);
				exit(1);
			}
			if(view_mode_arg) {
				fprintf(stderr, "Error: --sphere360 option can't be used together with the %s option\n", view_mode_arg);
				exit(1);
			}
			view_mode = ViewMode::SPHERE360;
			projection_mode = ProjectionMode::SPHERE360;
			projection_arg = argv[i];
			view_mode_arg = argv[i];
		} else if(strcmp(argv[i], "--stretch") == 0) {
			stretch = true;
		} else if(strcmp(argv[i], "--no-stretch") == 0) {
			stretch = false;
		} else if(strcmp(argv[i], "--cursor-wrap") == 0) {
			cursor_wrap = true;
			cursor_wrap_set = true;
		} else if(strcmp(argv[i], "--no-cursor-wrap") == 0) {
			cursor_wrap = false;
			cursor_wrap_set = true;
		} else if(strcmp(argv[i], "--video") == 0 && i < argc - 1) {
			mpv_file = argv[i + 1];
			++i;
			content_mode = ContentMode::MPV;
		} else if(strcmp(argv[i], "--use-system-mpv-config") == 0) {
			use_system_mpv_config = true;
		} else if(strcmp(argv[i], "--free-camera") == 0) {
			free_camera = true;
		} else if(argv[i][0] == '-') {
			fprintf(stderr, "Invalid flag: %s\n", argv[i]);
			usage();
		}
	}

	if(!zoom_set && projection_mode != ProjectionMode::SPHERE) {
		zoom = 1.0;
	}

	if(cursor_scale < 0.001f || (!cursor_scale_set && projection_mode == ProjectionMode::SPHERE)) {
		cursor_scale = 0.001f;
	}

	if(!cursor_wrap_set && projection_mode == ProjectionMode::FLAT) {
		cursor_wrap = false;
	}

	if(projection_mode == ProjectionMode::SPHERE360) {
		zoom = 0.0f;
		cursor_scale = 0.001f;
	}

	cursor_scale_uniform[0] = 0.0f;
	cursor_scale_uniform[1] = 0.0f;

#ifdef _DEBUG
	m_bDebugOpenGL = true;
#endif
};


//-----------------------------------------------------------------------------
// Purpose: Destructor
//-----------------------------------------------------------------------------
CMainApplication::~CMainApplication()
{
	// work is done in Shutdown
	dprintf( "Shutdown" );
}


//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
bool CMainApplication::BInit()
{
	pw_init(NULL, NULL);

	if ( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_JOYSTICK ) < 0 )
	{
		printf("%s - SDL could not initialize! SDL Error: %s\n", __FUNCTION__, SDL_GetError());
		return false;
	}

	// --- Create XrInstance
	uint32_t enabled_ext_count = 2;
	const char* enabled_exts[2] = {
		XR_KHR_OPENGL_ENABLE_EXTENSION_NAME,
		XR_MNDX_EGL_ENABLE_EXTENSION_NAME,
	};
	// same can be done for API layers, but API layers can also be enabled by env var

	XrInstanceCreateInfo instance_create_info = {
	    .type = XR_TYPE_INSTANCE_CREATE_INFO,
	    .next = NULL,
	    .createFlags = 0,
	    .applicationInfo =
	        {
	            // some compilers have trouble with char* initialization
	            .applicationName = {0},
	            .applicationVersion = 1,
	            .engineName = {0},
	            .engineVersion = 0,
	            .apiVersion = XR_CURRENT_API_VERSION,
	        },
	    .enabledApiLayerCount = 0,
	    .enabledApiLayerNames = NULL,
	    .enabledExtensionCount = enabled_ext_count,
	    .enabledExtensionNames = enabled_exts,
	};
	strncpy(instance_create_info.applicationInfo.applicationName, "OpenXR OpenGL Example",
	        XR_MAX_APPLICATION_NAME_SIZE);
	strncpy(instance_create_info.applicationInfo.engineName, "Custom", XR_MAX_ENGINE_NAME_SIZE);

	result = xrCreateInstance(&instance_create_info, &instance);
	if (!xr_check(NULL, result, "Failed to create XR instance."))
		return 1;

	if (!load_extension_function_pointers(instance))
		return 1;

	// --- Get XrSystemId
	XrSystemGetInfo system_get_info = {
	    .type = XR_TYPE_SYSTEM_GET_INFO, .next = NULL, .formFactor = form_factor};

	result = xrGetSystem(instance, &system_get_info, &system_id);
	if (!xr_check(instance, result, "Failed to get system for HMD form factor."))
		return 1;

	printf("Successfully got XrSystem with id %lu for HMD form factor\n", system_id);
	
	result = xrEnumerateViewConfigurationViews(instance, system_id, view_type, 0, &view_count, NULL);
	if (!xr_check(instance, result, "Failed to get view configuration view count!"))
		return 1;

	viewconfig_views = (XrViewConfigurationView *) malloc(sizeof(XrViewConfigurationView) * view_count);
	for (uint32_t i = 0; i < view_count; i++) {
		viewconfig_views[i].type = XR_TYPE_VIEW_CONFIGURATION_VIEW;
		viewconfig_views[i].next = NULL;
	}
	
	m_mat4LastViewRotation = (glm::mat4 *) malloc(sizeof(glm::mat4) * view_count);
	m_mat4LastViewRotationReset = (glm::mat4 *) malloc(sizeof(glm::mat4) * view_count);
	for (uint32_t i = 0; i < view_count; i++) {
		m_mat4LastViewRotation[i] = glm::mat4_cast(glm::quat(0.0f, 0.0f, 0.0f, 0.0f));
		m_mat4LastViewRotationReset[i] = glm::mat4_cast(glm::quat(0.0f, 0.0f, 0.0f, 0.0f));
	}

	result = xrEnumerateViewConfigurationViews(instance, system_id, view_type, view_count,
	                                           &view_count, viewconfig_views);
	if (!xr_check(instance, result, "Failed to enumerate view configuration views!"))
		return 1;


	
	// OpenXR requires checking graphics requirements before creating a session.
	XrGraphicsRequirementsOpenGLKHR opengl_reqs = {.type = XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_KHR,
	                                               .next = NULL};

	// this function pointer was loaded with xrGetInstanceProcAddr
	result = pfnGetOpenGLGraphicsRequirementsKHR(instance, system_id, &opengl_reqs);
	if (!xr_check(instance, result, "Failed to get OpenGL graphics requirements!"))
		return 1;

	/* Checking opengl_reqs.minApiVersionSupported and opengl_reqs.maxApiVersionSupported
	 * is not very useful, compatibility will depend on the OpenGL implementation and the
	 * OpenXR runtime much more than the OpenGL version.
	 * Other APIs have more useful verifiable requirements. */
	
	// --- Create session
	graphics_binding_gl = (XrGraphicsBindingEGLMNDX){
	    .type = XR_TYPE_GRAPHICS_BINDING_EGL_MNDX,
	};

	int nWindowPosX = 700;
	int nWindowPosY = 100;
	Uint32 unWindowFlags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;

	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 2 );
	//SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );

	SDL_GL_SetAttribute( SDL_GL_MULTISAMPLEBUFFERS, 0 );
	SDL_GL_SetAttribute( SDL_GL_MULTISAMPLESAMPLES, 0 );
	if( m_bDebugOpenGL )
		SDL_GL_SetAttribute( SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG );
	
	// Needed for mpv
	SDL_SetHint(SDL_HINT_NO_SIGNAL_HANDLERS, "no");

	m_pCompanionWindow = SDL_CreateWindow( "xr-video-player", nWindowPosX, nWindowPosY, m_nCompanionWindowWidth, m_nCompanionWindowHeight, unWindowFlags );
	if (m_pCompanionWindow == NULL)
	{
		printf( "%s - Window could not be created! SDL Error: %s\n", __FUNCTION__, SDL_GetError() );
		return false;
	}

	gl_context = SDL_GL_CreateContext(m_pCompanionWindow);
	if (gl_context == NULL)
	{
		printf( "%s - OpenGL context could not be created! SDL Error: %s\n", __FUNCTION__, SDL_GetError() );
		return false;
	}
	
	glewExperimental = GL_TRUE;
	GLenum nGlewError = glewInit();
	if (nGlewError != GLEW_OK)
	{
		if (GLEW_OK != nGlewError && nGlewError != GLEW_ERROR_NO_GLX_DISPLAY)
		{
			printf( "%s - Error initializing GLEW! %s\n", __FUNCTION__, glewGetErrorString( nGlewError ) );
			return false;
		}
	}
	glGetError(); // to clear the error caused deep in GLEW
	
	EGLDisplay display = eglGetCurrentDisplay();
	if (display == NULL) {
		display = eglGetDisplay(wl_display_connect(NULL));
	}
	graphics_binding_gl.display = display;
	graphics_binding_gl.context = eglGetCurrentContext();
	surface_draw = eglGetCurrentSurface(EGL_DRAW);
	surface_read = eglGetCurrentSurface(EGL_READ);
	EGLint * config = NULL;
	eglQueryContext(graphics_binding_gl.display, graphics_binding_gl.context, (EGLint) EGL_CONFIG_ID, config);
	graphics_binding_gl.config = config;
	graphics_binding_gl.getProcAddress = eglGetProcAddress;
	
	egl_data.display = display;
	egl_data.context = eglGetCurrentContext();
	egl_data.config = config;
	egl_data.surface = surface_read;
	egl_data.surface_draw = surface_draw;
	egl_data.mutex = &context_mutex;
	egl_data.mutex_counter = &context_mutex_counter;
	gl_egl_device_enter_context(&egl_data);
	if (SDL_GL_SetSwapInterval(m_bVblank ? 1 : 0) < 0)
	{
		printf( "%s - Warning: Unable to set VSync! SDL Error: %s\n", __FUNCTION__, SDL_GetError() );
		return false;
	}

	XrSessionCreateInfo session_create_info = {
	    .type = XR_TYPE_SESSION_CREATE_INFO, .next = &graphics_binding_gl, .systemId = system_id};

	result = xrCreateSession(instance, &session_create_info, &session);
	if (!xr_check(instance, result, "Failed to create session"))
		return 1;

	printf("Successfully created a session with OpenGL!\n");
	
	/* Many runtimes support at least STAGE and LOCAL but not all do.
	 * Sophisticated apps might check with xrEnumerateReferenceSpaces() if the
	 * chosen one is supported and try another one if not.
	 * Here we will get an error from xrCreateReferenceSpace() and exit. */
	XrReferenceSpaceCreateInfo play_space_create_info = {.type = XR_TYPE_REFERENCE_SPACE_CREATE_INFO,
	                                                     .next = NULL,
	                                                     .referenceSpaceType = play_space_type,
	                                                     .poseInReferenceSpace = identity_pose};

	result = xrCreateReferenceSpace(session, &play_space_create_info, &play_space);
	if (!xr_check(instance, result, "Failed to create play space!"))
		return 1;
	

	// cube array
 	m_fScale = 1.0f;
 
 	m_fNearClip = 0.01f;
 	m_fFarClip = 30.0f;
 
 	m_uiVertcount = 0;
	
	m_nRenderWidth = viewconfig_views[0].recommendedImageRectWidth;
	m_nRenderHeight = viewconfig_views[0].recommendedImageRectHeight;
 
// 		m_MillisecondsTimer.start(1, this);
// 		m_SecondsTimer.start(1000, this);

	// --- Create Swapchains
	uint32_t swapchain_format_count;
	result = xrEnumerateSwapchainFormats(session, 0, &swapchain_format_count, NULL);
	if (!xr_check(instance, result, "Failed to get number of supported swapchain formats"))
		return 1;

	printf("Runtime supports %d swapchain formats\n", swapchain_format_count);
	int64_t swapchain_formats[swapchain_format_count];
	result = xrEnumerateSwapchainFormats(session, swapchain_format_count, &swapchain_format_count,
	                                     swapchain_formats);
	if (!xr_check(instance, result, "Failed to enumerate swapchain formats"))
		return 1;

	// SRGB is usually a better choice than linear
	// a more sophisticated approach would iterate supported swapchain formats and choose from them
	int64_t color_format = get_swapchain_format(instance, session, GL_SRGB8_ALPHA8_EXT, true);

	// GL_DEPTH_COMPONENT16 is a good bet
	// SteamVR 1.16.4 supports GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT32
	// but NOT GL_DEPTH_COMPONENT32F
	int64_t depth_format = get_swapchain_format(instance, session, GL_DEPTH_COMPONENT16, false);
	if (depth_format < 0) {
		printf("Preferred depth format GL_DEPTH_COMPONENT16 not supported, disabling depth\n");
		depth.supported = false;
	}


	// --- Create swapchain for main VR rendering
	{
		// In the frame loop we render into OpenGL textures we receive from the runtime here.
		swapchains = (XrSwapchain*) malloc(sizeof(XrSwapchain) * view_count);
		swapchain_lengths = (uint32_t*) malloc(sizeof(uint32_t) * view_count);
		images = (XrSwapchainImageOpenGLKHR**) malloc(sizeof(XrSwapchainImageOpenGLKHR*) * view_count);
		for (uint32_t i = 0; i < view_count; i++) {
			XrSwapchainCreateInfo swapchain_create_info = {
			    .type = XR_TYPE_SWAPCHAIN_CREATE_INFO,
			    .next = NULL,
			    .createFlags = 0,
			    .usageFlags = XR_SWAPCHAIN_USAGE_SAMPLED_BIT | XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT,
			    .format = color_format,
			    .sampleCount = viewconfig_views[i].recommendedSwapchainSampleCount,
			    .width = viewconfig_views[i].recommendedImageRectWidth,
			    .height = viewconfig_views[i].recommendedImageRectHeight,
			    .faceCount = 1,
			    .arraySize = 1,
			    .mipCount = 1,
			};

			result = xrCreateSwapchain(session, &swapchain_create_info, &swapchains[i]);
			if (!xr_check(instance, result, "Failed to create swapchain %d!", i))
				return 1;

			// The runtime controls how many textures we have to be able to render to
			// (e.g. "triple buffering")
			result = xrEnumerateSwapchainImages(swapchains[i], 0, &swapchain_lengths[i], NULL);
			if (!xr_check(instance, result, "Failed to enumerate swapchains"))
				return 1;

			images[i] = (XrSwapchainImageOpenGLKHR*) malloc(sizeof(XrSwapchainImageOpenGLKHR) * swapchain_lengths[i]);
			for (uint32_t j = 0; j < swapchain_lengths[i]; j++) {
				images[i][j].type = XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_KHR;
				images[i][j].next = NULL;
			}
			fprintf(stderr, "swapchain_lengths: %d\n", swapchain_lengths[i]);
			assert(swapchain_lengths[i] >= 2);
			result =
			    xrEnumerateSwapchainImages(swapchains[i], swapchain_lengths[i], &swapchain_lengths[i],
			                               (XrSwapchainImageBaseHeader*)images[i]);
			if (!xr_check(instance, result, "Failed to enumerate swapchain images"))
				return 1;
		}
	}
	// --- Create swapchain for depth buffers if supported
	{
		if (depth.supported) {
			depth_swapchains = (XrSwapchain*) malloc(sizeof(XrSwapchain) * view_count);
			depth_swapchain_lengths = (uint32_t*) malloc(sizeof(uint32_t) * view_count);
			depth_images = (XrSwapchainImageOpenGLKHR**) malloc(sizeof(XrSwapchainImageOpenGLKHR*) * view_count);
			for (uint32_t i = 0; i < view_count; i++) {
				XrSwapchainCreateInfo swapchain_create_info = {
				    .type = XR_TYPE_SWAPCHAIN_CREATE_INFO,
				    .next = NULL,
				    .createFlags = 0,
				    .usageFlags = XR_SWAPCHAIN_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
				    .format = depth_format,
				    .sampleCount = viewconfig_views[i].recommendedSwapchainSampleCount,
				    .width = viewconfig_views[i].recommendedImageRectWidth,
				    .height = viewconfig_views[i].recommendedImageRectHeight,
				    .faceCount = 1,
				    .arraySize = 1,
				    .mipCount = 1,
				};

				result = xrCreateSwapchain(session, &swapchain_create_info, &depth_swapchains[i]);
				if (!xr_check(instance, result, "Failed to create swapchain %d!", i))
					return 1;

				result =
				    xrEnumerateSwapchainImages(depth_swapchains[i], 0, &depth_swapchain_lengths[i], NULL);
				if (!xr_check(instance, result, "Failed to enumerate swapchains"))
					return 1;

				// these are wrappers for the actual OpenGL texture id
				depth_images[i] = (XrSwapchainImageOpenGLKHR*) malloc(sizeof(XrSwapchainImageOpenGLKHR) * depth_swapchain_lengths[i]);
				for (uint32_t j = 0; j < depth_swapchain_lengths[i]; j++) {
					depth_images[i][j].type = XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_KHR;
					depth_images[i][j].next = NULL;
				}
				result = xrEnumerateSwapchainImages(depth_swapchains[i], depth_swapchain_lengths[i],
				                                    &depth_swapchain_lengths[i],
				                                    (XrSwapchainImageBaseHeader*)depth_images[i]);
				if (!xr_check(instance, result, "Failed to enumerate swapchain images"))
					return 1;
			}
		}
	}
	
	if (!BInitGL())
	{
		printf("%s - Unable to initialize OpenGL!\n", __FUNCTION__);
		return false;
	}
	
	loop = g_main_loop_new (NULL, TRUE);
	if (content_mode == ContentMode::PIPEWIRE) {
		window_texture = (struct screencast_portal_capture *) screencast_portal_capture_create(&egl_data);
	}


	// Do not allocate these every frame to save some resources
	views = (XrView*)malloc(sizeof(XrView) * view_count);
	for (uint32_t i = 0; i < view_count; i++) {
		views[i].type = XR_TYPE_VIEW;
		views[i].next = NULL;
	}
	

	projection_views = (XrCompositionLayerProjectionView*) malloc(sizeof(XrCompositionLayerProjectionView) * view_count);
	for (uint32_t i = 0; i < view_count; i++) {
		projection_views[i].type = XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW;
		projection_views[i].next = NULL;

		projection_views[i].subImage.swapchain = swapchains[i];
		projection_views[i].subImage.imageArrayIndex = 0;
		projection_views[i].subImage.imageRect.offset.x = 0;
		projection_views[i].subImage.imageRect.offset.y = 0;
		projection_views[i].subImage.imageRect.extent.width =
		    viewconfig_views[i].recommendedImageRectWidth;
		projection_views[i].subImage.imageRect.extent.height =
		    viewconfig_views[i].recommendedImageRectHeight;

		// projection_views[i].{pose, fov} have to be filled every frame in frame loop
	};
	
	if (content_mode == ContentMode::MPV) {
		mpv.init(&egl_data);
		mpv_thread = std::thread([&]{
			mpv.run(use_system_mpv_config, mpv_file, &m_unCompanionWindowVAO, &m_unCompanionWindowProgramID);
		});
	}
	gl_egl_device_leave_context(&egl_data);

	return true;
}


//-----------------------------------------------------------------------------
// Purpose: Outputs the string in message to debugging output.
//          All other parameters are ignored.
//          Does not return any meaningful value or reference.
//-----------------------------------------------------------------------------
void DebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const char* message, const void* userParam)
{
	dprintf( "GL Error: %s\n", message );
}


//-----------------------------------------------------------------------------
// Purpose: Initialize OpenGL. Returns true if OpenGL has been successfully
//          initialized, false if shaders could not be created.
//          If failure occurred in a module other than shaders, the function
//          may return true or throw an error. 
//-----------------------------------------------------------------------------
bool CMainApplication::BInitGL()
{
	
	gl_egl_device_enter_context(&egl_data);
	if( m_bDebugOpenGL )
	{
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback( (GLDEBUGPROC)DebugCallback, nullptr);
		glDebugMessageControl( GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE );
	}

	if( !CreateAllShaders() )
		return false;

	//glEnable(GL_CULL_FACE);

	glUseProgram( m_unSceneProgramID );

	//glActiveTexture(GL_TEXTURE0);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//glActiveTexture(GL_TEXTURE1);

    glBindTexture(GL_TEXTURE_2D, 0);

	glUniform1i(m_myTextureLocation, 0);
	glUniform1i(m_arrowTextureLocation, 1);
	glActiveTexture(GL_TEXTURE0);
	glUseProgram( 0);

	glGenVertexArrays( 1, &m_unSceneVAO );
	glGenBuffers( 1, &m_glSceneVertBuffer );

	framebuffers = (GLuint**) malloc(sizeof(GLuint*) * view_count);
	for (uint32_t i = 0; i < view_count; i++) {
		framebuffers[i] = (GLuint*) malloc(sizeof(GLuint) * swapchain_lengths[i]);
		glGenFramebuffers(swapchain_lengths[i], framebuffers[i]);
	}
	
	SetupScene();
	// if(!SetupStereoRenderTargets()) return false;
	SetupCompanionWindow();
	gl_egl_device_leave_context(&egl_data);

	return true;
}


//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
void CMainApplication::Shutdown()
{
	if( gl_context )
	{
		gl_egl_device_enter_context(&egl_data);
		if (content_mode == ContentMode::MPV) {
			if (mpv_thread.joinable())
				mpv_thread.join();
			mpv.destroy();
		}

		if( m_bDebugOpenGL )
		{
			glDebugMessageControl( GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_FALSE );
			glDebugMessageCallback(nullptr, nullptr);
		}
		glDeleteBuffers(1, &m_glSceneVertBuffer);

		if ( m_unSceneProgramID )
		{
			glDeleteProgram( m_unSceneProgramID );
		}
		if ( m_unCompanionWindowProgramID )
		{
			glDeleteProgram( m_unCompanionWindowProgramID );
		}

		glDeleteRenderbuffers( 1, &leftEyeDesc.m_nDepthBufferId );
		glDeleteTextures( 1, &leftEyeDesc.m_nRenderTextureId );
		glDeleteFramebuffers( 1, &leftEyeDesc.m_nRenderFramebufferId );
		glDeleteTextures( 1, &leftEyeDesc.m_nResolveTextureId );
		glDeleteFramebuffers( 1, &leftEyeDesc.m_nResolveFramebufferId );

		glDeleteRenderbuffers( 1, &rightEyeDesc.m_nDepthBufferId );
		glDeleteTextures( 1, &rightEyeDesc.m_nRenderTextureId );
		glDeleteFramebuffers( 1, &rightEyeDesc.m_nRenderFramebufferId );
		glDeleteTextures( 1, &rightEyeDesc.m_nResolveTextureId );
		glDeleteFramebuffers( 1, &rightEyeDesc.m_nResolveFramebufferId );

		if( m_unCompanionWindowVAO != 0 )
		{
			glDeleteVertexArrays( 1, &m_unCompanionWindowVAO );
		}
		if( m_unSceneVAO != 0 )
		{
			glDeleteVertexArrays( 1, &m_unSceneVAO );
		}
	}

	screencast_portal_capture_destroy(window_texture);
	pw_deinit();

	pthread_mutex_destroy(&context_mutex);
	if( m_pCompanionWindow )
	{
		SDL_DestroyWindow(m_pCompanionWindow);
		m_pCompanionWindow = NULL;
	}

	SDL_Quit();
}

void CMainApplication::zoom_in() {
    if(projection_mode == ProjectionMode::SPHERE360)
        zoom -= 1.0f;
    else
        zoom -= 0.01f;
    zoom_resize = true;

    std::stringstream strstr;
	strstr << "/tmp/xr-video-player.log";
    std::ofstream zoomstate(strstr.str());
    zoomstate << zoom; 
}

void CMainApplication::zoom_out() {
    if(projection_mode == ProjectionMode::SPHERE360)
        zoom += 1.0f;
    else
        zoom += 0.01f;
    zoom_resize = true;

    std::stringstream strstr;
	strstr << "/tmp/xr-video-player.log";
    std::ofstream zoomstate(strstr.str());
    zoomstate << zoom;
}

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
bool CMainApplication::HandleInput()
{
	SDL_Event sdlEvent;
	bool bRet = false;
    zoom_resize = false;
	bool mpv_quit = false;

	while ( SDL_PollEvent( &sdlEvent ) != 0 )
	{
		if ( sdlEvent.type == SDL_QUIT )
		{
			bRet = true;
		}
		else if ( sdlEvent.type == SDL_KEYDOWN )
		{
			if( sdlEvent.key.keysym.sym == SDLK_w )
			{
				m_bResetRotation = true;
			}
			if( sdlEvent.key.keysym.sym == SDLK_f )
			{
				m_bFreezRotation = !m_bFreezRotation;
			}
			if( sdlEvent.key.keysym.sym == SDLK_ESCAPE )
			{
				bRet = true;
			}
			if( sdlEvent.key.keysym.sym == SDLK_q )
			{
                zoom_in();
			}
			if( sdlEvent.key.keysym.sym == SDLK_e )
			{
                zoom_out();
			}
			if (content_mode == ContentMode::MPV) {
				switch (sdlEvent.key.keysym.sym) {
					case SDLK_LEFT:
						mpv.seek(-5.0); // Seek backwards 5 seconds
						break;
					case SDLK_RIGHT:
						mpv.seek(5.0); // Seek forwards 5 seconds
						break;
					case SDLK_SPACE:
						mpv.toggle_pause();
						break;
				}
			}
		}
		if(content_mode == ContentMode::MPV) {
			int error = 0;
			mpv.on_event(sdlEvent, &mpv_quit, &error);
			if(mpv_quit && error != 0)
				exit_code = 2;

			if(mpv_quit) {
				printf("MPV quit\n");
				bRet = true;
			}

			// TODO: Allow video resize to update texture size
			if(true
				&& mpv.video_width > 0 && mpv.video_height > 0 
				&& mpv.video_loaded
				&& (false
					|| mpv.video_width != pixmap_texture_width
					|| mpv.video_height != pixmap_texture_height
					|| !got_texture
				)
			) {
				printf("Update size from %d %d to %ld %ld\n", window_width, window_height, mpv.video_width, mpv.video_height);
				fflush(stdout);
				window_width = mpv.video_width;
				window_height = mpv.video_height;
				pixmap_texture_width = window_width;
				pixmap_texture_height = window_height;
				got_texture = true;
				SetupScene();
			}
		}
	}

	if (content_mode == ContentMode::PIPEWIRE) {
		gboolean may_block = false;
		g_main_context_iteration(g_main_loop_get_context(loop), may_block);
		// SDL_GL_DeleteContext(gl_context);
		// glXMakeCurrent(graphics_binding_gl.xDisplay, None, NULL);
		// g_main_loop_run (loop);
		// eglMakeCurrent(graphics_binding_gl.display, surface_draw, surface_read, graphics_binding_gl.context);
	
		focused_window_changed = false
			|| window_width != screencast_portal_capture_get_width(window_texture)
			|| window_height != screencast_portal_capture_get_height(window_texture)
		;
		// printf("Rebuild Scene?: %d %d\n", screencast_portal_capture_get_width(window_texture), screencast_portal_capture_get_height(window_texture));
		
		Uint32 time_now = SDL_GetTicks();
		const int window_resize_timeout = 500; /* 0.5 seconds */
		if((focused_window_changed || !got_texture) && (time_now - window_resize_time >= window_resize_timeout)) {
			window_resize_time = SDL_GetTicks();

			// window_texture_deinit(&window_texture);
			if(screencast_portal_capture_get_texture(window_texture) == 0) {
				fprintf(stderr, "Failed to init texture\n");
				//return false;
			} else {
				got_texture = true;
				screencast_portal_capture_show(window_texture);
				focused_window_changed = false;
				window_width = screencast_portal_capture_get_width(window_texture);
				window_height = screencast_portal_capture_get_height(window_texture);
				pixmap_texture_width = window_width;
				pixmap_texture_height = window_height;
				printf("Rebuild Scene\n");
				fflush(stdout);
				SetupScene();
			}
		} else if(!window_resized && zoom_resize) {
			SetupScene();
		}
	} else {
		if(zoom_resize) {
			SetupScene();
		}
	}

	if(m_bResetRotation) {
		printf("reset rotation!\n");
		m_bResetRotation = false;
		for (uint32_t i = 0; i < view_count; i++) {
			m_mat4LastViewRotationReset[i] = m_mat4LastViewRotation[i];
		}
	}
	return bRet;
}

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
int CMainApplication::RunMainLoop()
{

	SDL_StartTextInput();

	SDL_Joystick *controller = SDL_JoystickOpen(0);
	if (!controller)
		fprintf(stderr, "Could not open gamecontroller: %s\n", SDL_GetError());


	XrSessionState state = XR_SESSION_STATE_UNKNOWN;
	
	bool quit_mainloop = false;
	bool session_running = false; // to avoid beginning an already running session
	bool run_framecycle = false;  // for some session states skip the frame cycle
	while ( !quit_mainloop )
	{
		quit_mainloop = HandleInput();
		
		// --- Handle runtime Events
		// we do this before xrWaitFrame() so we can go idle or
		// break out of the main render loop as early as possible and don't have to
		// uselessly render or submit one. Calling xrWaitFrame commits you to
		// calling xrBeginFrame eventually.
		XrEventDataBuffer runtime_event = {.type = XR_TYPE_EVENT_DATA_BUFFER, .next = NULL};
		XrResult poll_result = xrPollEvent(instance, &runtime_event);
		while (poll_result == XR_SUCCESS) {
			switch (runtime_event.type) {
			case XR_TYPE_EVENT_DATA_INSTANCE_LOSS_PENDING: {
				XrEventDataInstanceLossPending* event = (XrEventDataInstanceLossPending*)&runtime_event;
				printf("EVENT: instance loss pending at %lu! Destroying instance.\n", event->lossTime);
				quit_mainloop = true;
				continue;
			}
			case XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED: {
				XrEventDataSessionStateChanged* event = (XrEventDataSessionStateChanged*)&runtime_event;
				printf("EVENT: session state changed from %d to %d\n", state, event->state);
				state = event->state;

				/*
				 * react to session state changes, see OpenXR spec 9.3 diagram. What we need to react to:
				 *
				 * * READY -> xrBeginSession STOPPING -> xrEndSession (note that the same session can be restarted)
				 * * EXITING -> xrDestroySession (EXITING only happens after we went through STOPPING and called xrEndSession)
				 *
				 * After exiting it is still possible to create a new session but we don't do that here.
				 *
				 * * IDLE -> don't run render loop, but keep polling for events
				 * * SYNCHRONIZED, VISIBLE, FOCUSED -> run render loop
				 */
				switch (state) {
				// skip render loop, keep polling
				case XR_SESSION_STATE_MAX_ENUM: // must be a bug
				case XR_SESSION_STATE_IDLE:
				case XR_SESSION_STATE_UNKNOWN: {
					run_framecycle = false;

					break; // state handling switch
				}

				// do nothing, run render loop normally
				case XR_SESSION_STATE_FOCUSED:
				case XR_SESSION_STATE_SYNCHRONIZED:
				case XR_SESSION_STATE_VISIBLE: {
					run_framecycle = true;

					break; // state handling switch
				}

				// begin session and then run render loop
				case XR_SESSION_STATE_READY: {
					// start session only if it is not running, i.e. not when we already called xrBeginSession
					// but the runtime did not switch to the next state yet
					if (!session_running) {
						XrSessionBeginInfo session_begin_info = {.type = XR_TYPE_SESSION_BEGIN_INFO,
						                                         .next = NULL,
						                                         .primaryViewConfigurationType = view_type};
						result = xrBeginSession(session, &session_begin_info);
						if (!xr_check(instance, result, "Failed to begin session!"))
							return 1;
						printf("Session started!\n");
						session_running = true;
					}
					// after beginning the session, run render loop
					run_framecycle = true;

					break; // state handling switch
				}

				// end session, skip render loop, keep polling for next state change
				case XR_SESSION_STATE_STOPPING: {
					// end session only if it is running, i.e. not when we already called xrEndSession but the
					// runtime did not switch to the next state yet
					if (session_running) {
						result = xrEndSession(session);
						if (!xr_check(instance, result, "Failed to end session!"))
							return 1;
						session_running = false;
					}
					// after ending the session, don't run render loop
					run_framecycle = false;

					break; // state handling switch
				}

				// destroy session, skip render loop, exit render loop and quit
				case XR_SESSION_STATE_LOSS_PENDING:
				case XR_SESSION_STATE_EXITING:
					result = xrDestroySession(session);
					if (!xr_check(instance, result, "Failed to destroy session!"))
						return 1;
					quit_mainloop = true;
					run_framecycle = false;

					break; // state handling switch
				}
				break; // session event handling switch
			}
			case XR_TYPE_EVENT_DATA_INTERACTION_PROFILE_CHANGED: {
				printf("EVENT: interaction profile changed!\n");
				XrEventDataInteractionProfileChanged* event =
				    (XrEventDataInteractionProfileChanged*)&runtime_event;
				(void)event;

				XrInteractionProfileState state = {.type = XR_TYPE_INTERACTION_PROFILE_STATE};

				for (int i = 0; i < HAND_COUNT; i++) {
					XrResult res = xrGetCurrentInteractionProfile(session, hand_paths[i], &state);
					if (!xr_check(instance, res, "Failed to get interaction profile for %d", i))
						continue;

					XrPath prof = state.interactionProfile;

					uint32_t strl;
					char profile_str[XR_MAX_PATH_LENGTH];
					res = xrPathToString(instance, prof, XR_MAX_PATH_LENGTH, &strl, profile_str);
					if (!xr_check(instance, res, "Failed to get interaction profile path str for %d", i))
						continue;

					printf("Event: Interaction profile changed for %d: %s\n", i, profile_str);
				}
				break;
			}
			default: printf("Unhandled event (type %d)\n", runtime_event.type);
			}

			runtime_event.type = XR_TYPE_EVENT_DATA_BUFFER;
			poll_result = xrPollEvent(instance, &runtime_event);
		}
		if (poll_result == XR_EVENT_UNAVAILABLE) {
			// processed all events in the queue
		} else {
			printf("Failed to poll events!\n");
			break;
		}

		if (!run_framecycle) {
			continue;
		}

		// --- Wait for our turn to do head-pose dependent computation and render a frame
		XrFrameState frame_state = {.type = XR_TYPE_FRAME_STATE, .next = NULL};
		XrFrameWaitInfo frame_wait_info = {.type = XR_TYPE_FRAME_WAIT_INFO, .next = NULL};
		result = xrWaitFrame(session, &frame_wait_info, &frame_state);
		if (!xr_check(instance, result, "xrWaitFrame() was not successful, exiting..."))
			break;



		// --- Create projection matrices and view matrices for each eye
		XrViewLocateInfo view_locate_info = {.type = XR_TYPE_VIEW_LOCATE_INFO,
		                                     .next = NULL,
		                                     .viewConfigurationType =
		                                         XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO,
		                                     .displayTime = frame_state.predictedDisplayTime,
		                                     .space = play_space};

		XrViewState view_state = {.type = XR_TYPE_VIEW_STATE, .next = NULL};
		result = xrLocateViews(session, &view_locate_info, &view_state, view_count, &view_count, views);
		if (!xr_check(instance, result, "Could not locate views"))
			break;
		
		XrSpaceLocation hand_locations[HAND_COUNT];

		// --- Begin frame
		XrFrameBeginInfo frame_begin_info = {.type = XR_TYPE_FRAME_BEGIN_INFO, .next = NULL};

		result = xrBeginFrame(session, &frame_begin_info);
		if (!xr_check(instance, result, "failed to begin frame!"))
			break;


		gl_egl_device_enter_context(&egl_data);
		// render each eye and fill projection_views with the result
		for (uint32_t i = 0; i < view_count; i++) {

			if (!frame_state.shouldRender) {
				printf("shouldRender = false, Skipping rendering work\n");
				continue;
			}

			glm::mat4 projection_matrix;
			XrMatrix4x4f_CreateProjectionFov(&projection_matrix, GRAPHICS_OPENGL, views[i].fov, m_fNearClip, m_fFarClip);

			glm::mat4 orientation = glm::mat4_cast(glm::quat(
				views[i].pose.orientation.w, 
				-views[i].pose.orientation.x, 
				-views[i].pose.orientation.y, 
				-views[i].pose.orientation.z
			));
			
			if (m_bFreezRotation) {
				orientation = m_mat4LastViewRotation[i];
			} else {
				m_mat4LastViewRotation[i] = orientation;
			}
			orientation *= glm::inverse(m_mat4LastViewRotationReset[i]);
			
			glm::mat4 position = glm::mat4(
				1.0f, 0.0f, 0.0f, 0.0f, 
				0.0f, 1.0f, 0.0f, 0.0f, 
				0.0f, 0.0f, 1.0f, 0.0f, 
				views[i].pose.position.x, views[i].pose.position.y, views[i].pose.position.z, 1.0f
			);
			
			glm::mat4 view_matrix = projection_matrix * position * orientation;

			XrSwapchainImageAcquireInfo acquire_info = {.type = XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO,
			                                            .next = NULL};
			uint32_t acquired_index;
			result = xrAcquireSwapchainImage(swapchains[i], &acquire_info, &acquired_index);
			if (!xr_check(instance, result, "failed to acquire swapchain image!"))
				break;

			XrSwapchainImageWaitInfo wait_info = {
			    .type = XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO, .next = NULL, .timeout = 1000};
			result = xrWaitSwapchainImage(swapchains[i], &wait_info);
			if (!xr_check(instance, result, "failed to wait for swapchain image!"))
				break;

			uint32_t depth_acquired_index = UINT32_MAX;
			if (depth.supported) {
				XrSwapchainImageAcquireInfo depth_acquire_info = {
				    .type = XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO, .next = NULL};
				result = xrAcquireSwapchainImage(depth_swapchains[i], &depth_acquire_info,
				                                 &depth_acquired_index);
				if (!xr_check(instance, result, "failed to acquire swapchain image!"))
					break;

				XrSwapchainImageWaitInfo depth_wait_info = {
				    .type = XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO, .next = NULL, .timeout = 1000};
				result = xrWaitSwapchainImage(depth_swapchains[i], &depth_wait_info);
				if (!xr_check(instance, result, "failed to wait for swapchain image!"))
					break;
			}

			projection_views[i].pose = views[i].pose;
			projection_views[i].fov = views[i].fov;

			GLuint depth_image = depth.supported ? depth_images[i][depth_acquired_index].image : 0;

			int w = viewconfig_views[i].recommendedImageRectWidth;
			int h = viewconfig_views[i].recommendedImageRectHeight;

			// TODO: should not be necessary, but is for SteamVR 1.16.4 (but not 1.15.x)
			// glXMakeCurrent(graphics_binding_gl.xDisplay, graphics_binding_gl.glxDrawable, graphics_binding_gl.glxContext);
			// SDL_GL_MakeCurrent(m_pCompanionWindow, gl_context);
			// eglMakeCurrent(graphics_binding_gl.display, surface_draw, surface_read, graphics_binding_gl.context);
			
			
			FramebufferDesc* description = i == 0 ? &leftEyeDesc: &rightEyeDesc;

			description->m_nRenderTextureId = images[i][acquired_index].image;
			description->m_nResolveTextureId = images[i][acquired_index].image;
			description->m_nRenderFramebufferId = framebuffers[i][acquired_index];
			description->m_nResolveFramebufferId = framebuffers[i][acquired_index];
			// assert(description->m_nRenderTextureId != 0);
			
			RenderFrameView(
				i, 
				hand_locations, 
				projection_matrix,
				view_matrix,
				description
			);

			XrSwapchainImageReleaseInfo release_info = {.type = XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO,
			                                            .next = NULL};
			result = xrReleaseSwapchainImage(swapchains[i], &release_info);
			if (!xr_check(instance, result, "failed to release swapchain image!"))
				break;

			if (depth.supported) {
				XrSwapchainImageReleaseInfo depth_release_info = {
				    .type = XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO, .next = NULL};
				result = xrReleaseSwapchainImage(depth_swapchains[i], &depth_release_info);
				if (!xr_check(instance, result, "failed to release swapchain image!"))
					break;
			}
		}
		RenderFrame();
		gl_egl_device_leave_context(&egl_data);

		XrCompositionLayerProjection projection_layer = {
		    .type = XR_TYPE_COMPOSITION_LAYER_PROJECTION,
		    .next = NULL,
		    .layerFlags = 0,
		    .space = play_space,
		    .viewCount = view_count,
		    .views = projection_views,
		};

		uint32_t submitted_layer_count = 1;
		const XrCompositionLayerBaseHeader* submitted_layers[1] = {
		    (const XrCompositionLayerBaseHeader* const) & projection_layer};

		if ((view_state.viewStateFlags & XR_VIEW_STATE_ORIENTATION_VALID_BIT) == 0) {
			printf("submitting 0 layers because orientation is invalid\n");
			submitted_layer_count = 0;
		}

		if (!frame_state.shouldRender) {
			printf("submitting 0 layers because shouldRender = false\n");
			submitted_layer_count = 0;
		}

		XrFrameEndInfo frameEndInfo = {.type = XR_TYPE_FRAME_END_INFO,
		                               .next = NULL,
		                               .displayTime = frame_state.predictedDisplayTime,
		                               .environmentBlendMode = XR_ENVIRONMENT_BLEND_MODE_OPAQUE,
		                               .layerCount = submitted_layer_count,
		                               .layers = submitted_layers,
			
		};
		result = xrEndFrame(session, &frameEndInfo);
		if (!xr_check(instance, result, "failed to end frame!"))
			break;
	}

	if (content_mode == ContentMode::MPV) {
		mpv.set_running(false);
		if(mpv_thread.joinable())
			mpv_thread.join();
	}

	if (controller)
		SDL_JoystickClose(controller);

	SDL_StopTextInput();
	return 1;
}


//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
void CMainApplication::RenderFrame()
{
	gl_egl_device_enter_context(&egl_data);
	// for now as fast as possible
	RenderCompanionWindow();

	if (m_bVblank)
	{
		//$ HACKHACK. From gpuview profiling, it looks like there is a bug where two renders and a present
		// happen right before and after the vsync causing all kinds of jittering issues. This glFinish()
		// appears to clear that up. Temporary fix while I try to get nvidia to investigate this problem.
		// 1/29/2014 mikesart
		glFinish();
	}

	// SwapWindow
	{
		SDL_GL_SwapWindow(m_pCompanionWindow);
	}

	// Clear
	{
		// We want to make sure the glFinish waits for the entire present to complete, not just the submission
		// of the command. So, we do a clear here right here so the glFinish will wait fully for the swap.
		glClearColor(0, 0, 0, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	// Flush and wait for swap.
	if (m_bVblank) {
		glFlush();
		glFinish();
	}
	gl_egl_device_leave_context(&egl_data);
}

//-----------------------------------------------------------------------------
// Purpose: resets rotation & position of the screen
//-----------------------------------------------------------------------------
void CMainApplication::ResetRotation()
{
	m_bResetRotation = true;
}


//-----------------------------------------------------------------------------
// Purpose: Compiles a GL shader program and returns the handle. Returns 0 if
//			the shader couldn't be compiled for some reason.
//-----------------------------------------------------------------------------
GLuint CMainApplication::CompileGLShader( const char *pchShaderName, const char *pchVertexShader, const char *pchFragmentShader )
{
	GLuint unProgramID = glCreateProgram();

	GLuint nSceneVertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource( nSceneVertexShader, 1, &pchVertexShader, NULL);
	glCompileShader( nSceneVertexShader );

	GLint vShaderCompiled = GL_FALSE;
	glGetShaderiv( nSceneVertexShader, GL_COMPILE_STATUS, &vShaderCompiled);
	if ( vShaderCompiled != GL_TRUE)
	{
		dprintf("%s - Unable to compile vertex shader %d!\n", pchShaderName, nSceneVertexShader);
		glDeleteProgram( unProgramID );
		glDeleteShader( nSceneVertexShader );
		return 0;
	}
	glAttachShader( unProgramID, nSceneVertexShader);
	glDeleteShader( nSceneVertexShader ); // the program hangs onto this once it's attached

	GLuint  nSceneFragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource( nSceneFragmentShader, 1, &pchFragmentShader, NULL);
	glCompileShader( nSceneFragmentShader );

	GLint fShaderCompiled = GL_FALSE;
	glGetShaderiv( nSceneFragmentShader, GL_COMPILE_STATUS, &fShaderCompiled);
	if (fShaderCompiled != GL_TRUE)
	{
		dprintf("%s - Unable to compile fragment shader %d!\n", pchShaderName, nSceneFragmentShader );
		glDeleteProgram( unProgramID );
		glDeleteShader( nSceneFragmentShader );
		return 0;	
	}

	glAttachShader( unProgramID, nSceneFragmentShader );
	glDeleteShader( nSceneFragmentShader ); // the program hangs onto this once it's attached

	glLinkProgram( unProgramID );

	GLint programSuccess = GL_TRUE;
	glGetProgramiv( unProgramID, GL_LINK_STATUS, &programSuccess);
	if ( programSuccess != GL_TRUE )
	{
		dprintf("%s - Error linking program %d!\n", pchShaderName, unProgramID);
		glDeleteProgram( unProgramID );
		return 0;
	}

	glUseProgram( unProgramID );
	glUseProgram( 0 );

	return unProgramID;
}


//-----------------------------------------------------------------------------
// Purpose: Creates all the shaders used by HelloVR SDL
//-----------------------------------------------------------------------------
bool CMainApplication::CreateAllShaders()
{
	m_unSceneProgramID = CompileGLShader( 
		"Scene",

		// Vertex Shader
		"#version 410\n"
		"uniform mat4 matrix;\n"
		"uniform float texture_offset_x;\n"
		"uniform float texture_scale_x;\n"
		"uniform vec2 cursor_location;\n"
		"uniform vec2 arrow_size;\n"
		"layout(location = 0) in vec4 position;\n"
		"layout(location = 1) in vec2 v2UVcoordsIn;\n"
		"layout(location = 2) in vec3 v3NormalIn;\n"
		"out vec2 v2CursorLocation;\n"
		"out vec2 arrow_size_frag;\n"
		"out vec2 v2UVcoords;\n"
		"void main()\n"
		"{\n"
		"	v2UVcoords = vec2(1.0 - v2UVcoordsIn.x, v2UVcoordsIn.y) * vec2(texture_scale_x, 1.0) + vec2(texture_offset_x, 0.0);\n"
		"   vec4 inverse_pos = vec4(position.x, position.y, -position.z, position.w);\n"
		"	v2CursorLocation = cursor_location;\n"
		"	arrow_size_frag = arrow_size;\n"
		"	gl_Position = matrix * inverse_pos;\n"
		"}\n",

		// Fragment Shader
		"#version 410 core\n"
		"uniform sampler2D mytexture;\n"
		"uniform sampler2D arrow_texture;\n"
		"in vec2 v2UVcoords;\n"
		"in vec2 v2CursorLocation;\n"
		"in vec2 arrow_size_frag;\n"
		"out vec4 outputColor;\n"
		"void main()\n"
		"{\n"
		"	vec2 cursor_diff = (v2CursorLocation + arrow_size_frag) - v2UVcoords;\n"
		"	vec2 arrow_coord = (arrow_size_frag - cursor_diff) / arrow_size_frag;\n"
		"	vec4 arrow_col = texture(arrow_texture, arrow_coord);\n"
		"	vec4 col = texture(mytexture, v2UVcoords);\n"
		"	if(arrow_size_frag.x < 0.01 || arrow_size_frag.y < 0.01 || arrow_coord.x < 0.0 || arrow_coord.x > 1.0 || arrow_coord.y < 0.0 || arrow_coord.y > 1.0) arrow_col.a = 0.0;\n"
		"	outputColor = mix(col, arrow_col.bgra, arrow_col.a);\n"
		"}\n"
		);
	m_nSceneMatrixLocation = glGetUniformLocation( m_unSceneProgramID, "matrix" );
	if( m_nSceneMatrixLocation == -1 )
	{
		dprintf( "Unable to find matrix uniform in scene shader\n" );
		return false;
	}
	m_nSceneTextureOffsetXLocation = glGetUniformLocation( m_unSceneProgramID, "texture_offset_x" );
	if( m_nSceneTextureOffsetXLocation == -1 )
	{
		dprintf( "Unable to find texture_offset_x uniform in scene shader\n" );
		return false;
	}
	m_nSceneTextureScaleXLocation = glGetUniformLocation( m_unSceneProgramID, "texture_scale_x" );
	if( m_nSceneTextureScaleXLocation == -1 )
	{
		dprintf( "Unable to find texture_scale_x uniform in scene shader\n" );
		return false;
	}
	m_nCursorLocation = glGetUniformLocation( m_unSceneProgramID, "cursor_location" );
	if( m_nCursorLocation == -1 )
	{
		dprintf( "Unable to find cursor_location uniform in scene shader\n" );
		return false;
	}
	m_nArrowSizeLocation = glGetUniformLocation( m_unSceneProgramID, "arrow_size" );
	if( m_nArrowSizeLocation == -1 )
	{
		dprintf( "Unable to find arrow_size uniform in scene shader\n" );
		return false;
	}
	m_myTextureLocation = glGetUniformLocation(m_unSceneProgramID, "mytexture");
	if(m_myTextureLocation == -1) {
		dprintf( "Unable to find mytexture uniform in scene shader\n" );
		return false;
	}
	m_arrowTextureLocation = glGetUniformLocation(m_unSceneProgramID, "arrow_texture");
	if(m_arrowTextureLocation == -1) {
		dprintf( "Unable to find arrow_texture uniform in scene shader\n" );
		return false;
	}

	m_unCompanionWindowProgramID = CompileGLShader(
		"CompanionWindow",

		// vertex shader
		"#version 410 core\n"
		"layout(location = 0) in vec4 position;\n"
		"layout(location = 1) in vec2 v2UVIn;\n"
		"noperspective out vec2 v2UV;\n"
		"void main()\n"
		"{\n"
		"	v2UV = vec2(v2UVIn.x, 1.0 - v2UVIn.y);\n"
		"	gl_Position = position;\n"
		"}\n",

		// fragment shader
		"#version 410 core\n"
		"uniform sampler2D mytexture;\n"
		"noperspective in vec2 v2UV;\n"
		"out vec4 outputColor;\n"
		"void main()\n"
		"{\n"
		"	vec4 col = texture(mytexture, v2UV);\n"
		"	outputColor = col.rgba;\n"
		"}\n"
		);

	return m_unSceneProgramID != 0 
		&& m_unCompanionWindowProgramID != 0;
}

//-----------------------------------------------------------------------------
// Purpose: create a sea of cubes
//-----------------------------------------------------------------------------
void CMainApplication::SetupScene()
{
	gl_egl_device_enter_context(&egl_data);

	std::vector<float> vertdataarray;
#if 0
	glm::mat4 matScale =glm::scale(glm::mat4(1.0f), glm::vec3(m_fScale, m_fScale, m_fScale));
	glm::mat4 matTransform = glm::translate(glm::mat4(1.0f),
		glm::vec3(
			-( (float)m_iSceneVolumeWidth * m_fScaleSpacing ) / 2.f,
			-( (float)m_iSceneVolumeHeight * m_fScaleSpacing ) / 2.f,
			-( (float)m_iSceneVolumeDepth * m_fScaleSpacing ) / 2.f)
	);
	
	glm::mat4 mat = matScale * matTransform;

	for( int z = 0; z< m_iSceneVolumeDepth; z++ )
	{
		for( int y = 0; y< m_iSceneVolumeHeight; y++ )
		{
			for( int x = 0; x< m_iSceneVolumeWidth; x++ )
			{
				AddCubeToScene( mat, vertdataarray );
				mat = mat * glm::translate(glm::mat4(1.0f), glm::vec3(m_fScaleSpacing, 0, 0 ));
			}
			mat = mat * glm::translate(glm::mat4(1.0f), glm::vec3(-((float)m_iSceneVolumeWidth) * m_fScaleSpacing, m_fScaleSpacing, 0 ));
		}
		mat = mat * glm::translate(glm::mat4(1.0f), glm::vec3(0, -((float)m_iSceneVolumeHeight) * m_fScaleSpacing, m_fScaleSpacing ));
	}

#else
	glm::mat4 matScale = glm::mat4(1.0f);
	matScale = glm::scale(matScale, glm::vec3(m_fScale, m_fScale, m_fScale));
	glm::mat4 matTransform = glm::mat4(1.0f);
	/*
	matTransform = glm::translate(glm::mat4(1.0f),
		glm::vec3(-m_fScale*0.5f, -m_fScale*0.5f, 0.5f)
	);
	*/
	
	glm::mat4 mat = matScale * matTransform;
	AddCubeToScene( mat, vertdataarray );
#endif
	m_uiVertcount = vertdataarray.size()/5;
	
	glBindVertexArray( m_unSceneVAO );
	glBindBuffer( GL_ARRAY_BUFFER, m_glSceneVertBuffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(float) * vertdataarray.size(), &vertdataarray[0], GL_STATIC_DRAW);

	GLsizei stride = sizeof(VertexDataScene);
	uintptr_t offset = 0;

	glEnableVertexAttribArray( 0 );
	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, stride , (const void *)offset);

	offset += sizeof(glm::vec3);
	glEnableVertexAttribArray( 1 );
	glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, stride, (const void *)offset);

	glBindVertexArray( 0 );
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	gl_egl_device_leave_context(&egl_data);
}


static void AddCubeVertex( float x, float y, float z, float u, float v, std::vector<float> &vertdata )
{
	vertdata.push_back( x );
	vertdata.push_back( y );
	vertdata.push_back( z );
	vertdata.push_back( u );
	vertdata.push_back( v );
}

static void CreateSegmentedPlane(std::vector<float> &vertdata, float width, float height, float depth, float texture_width, float texture_height, float texture_offset_x, float texture_offset_y, int num_columns, int num_rows) {
	#if 0
	float segment_width = width / (float)num_columns;
	float segment_height = height / (float)num_rows;
	float segment_texture_width = texture_width / (float)num_columns;
	float segment_texture_height = texture_height / (float)num_rows;
	for(int y = 0; y < num_rows; ++y) {
		float segment_height_offset = segment_height * (float)y;
		float segment_texture_height_offset = segment_texture_height * (float)y;
		for(int x = 0; x < num_columns; ++x) {
			float segment_width_offset = segment_width * (float)x;
			float segment_texture_width_offset = segment_texture_width * (float)x;
			//segment_texture_width_offset = 0.0f;
			//segment_texture_height_offset = 0.0f;
			//segment_texture_width = texture_width;
			//segment_texture_height = texture_height;
			// AddCubeVertex(segment_width_offset + -segment_width, 	segment_height_offset +  segment_height, zoom, segment_texture_width_offset + texture_offset_x + segment_texture_width, 	segment_texture_height_offset + texture_offset_y, 							vertdata);
			// AddCubeVertex(segment_width_offset + segment_width, 	segment_height_offset +  segment_height, zoom, segment_texture_width_offset + texture_offset_x, 							segment_texture_height_offset + texture_offset_y, 							vertdata);
			// AddCubeVertex(segment_width_offset + -segment_width, 	segment_height_offset + -segment_height, zoom, segment_texture_width_offset + texture_offset_x + segment_texture_width, 	segment_texture_height_offset + texture_offset_y + segment_texture_height, 	vertdata);

			// AddCubeVertex(segment_width_offset + -segment_width, 	segment_height_offset + -segment_height, zoom, segment_texture_width_offset + texture_offset_x + segment_texture_width, 	segment_texture_height_offset + texture_offset_y + segment_texture_height, 	vertdata);
			// AddCubeVertex(segment_width_offset + segment_width, 	segment_height_offset + -segment_height, zoom, segment_texture_width_offset + texture_offset_x, 							segment_texture_height_offset + texture_offset_y + segment_texture_height, 	vertdata);
			// AddCubeVertex(segment_width_offset + segment_width, 	segment_height_offset +  segment_height, zoom, segment_texture_width_offset + texture_offset_x, 							segment_texture_height_offset + texture_offset_y, 							vertdata);
		}
	}
	#endif
	float segment_width = width / (float)num_columns;
	float segment_height = height / (float)num_rows;
	float segment_texture_width = texture_width / (float)num_columns;
	float segment_texture_height = texture_height / (float)num_rows;

	for(int y = 0; y < num_rows; ++y) {
		float segment_height_offset = height - segment_height * 2.0f * (float)y;
		float segment_texture_height_offset = segment_texture_height * (float)y;
		for(int x = 0; x < num_columns; ++x) {
			float segment_width_offset = width - segment_width * 2.0f * (float)x;
			float segment_texture_width_offset = segment_texture_width * (float)x;

			AddCubeVertex(segment_width_offset, segment_height_offset, depth, segment_texture_width_offset + texture_offset_x, segment_texture_height_offset + texture_offset_y, vertdata);
			AddCubeVertex(segment_width_offset - segment_width*2.0f, segment_height_offset, depth, segment_texture_width_offset + texture_offset_x + segment_texture_width, segment_texture_height_offset + texture_offset_y, vertdata);
			AddCubeVertex(segment_width_offset - segment_width*2.0f, segment_height_offset - segment_height*2.0f, depth, segment_texture_width_offset + texture_offset_x + segment_texture_width, segment_texture_height_offset + texture_offset_y + segment_texture_height, vertdata);

			AddCubeVertex(segment_width_offset - segment_width*2.0f, segment_height_offset - segment_height*2.0f, depth, segment_texture_width_offset + texture_offset_x + segment_texture_width, segment_texture_height_offset + texture_offset_y + segment_texture_height, vertdata);
			AddCubeVertex(segment_width_offset, segment_height_offset - segment_height*2.0f, depth, segment_texture_width_offset + texture_offset_x, segment_texture_height_offset + texture_offset_y + segment_texture_height, vertdata);
			AddCubeVertex(segment_width_offset, segment_height_offset, depth, segment_texture_width_offset + texture_offset_x, segment_texture_height_offset + texture_offset_y, vertdata);
		}
	}
}

static glm::vec3 vertex_get_center(glm::mat3 vertex) {
	return glm::vec3(
		(vertex[0].x + vertex[1].x + vertex[2].x) / 3.0f,
		(vertex[0].y + vertex[1].y + vertex[2].y) / 3.0f,
		(vertex[0].z + vertex[1].z + vertex[2].z) / 3.0f
	);
}

static void plane_normalize_depth(float *vertices, size_t num_vertices, float depth) {
	for(size_t i = 0; i < num_vertices; ++i) {
		float *vertex_data = &vertices[i * 5];
		float dist = sqrtf(vertex_data[0]*vertex_data[0] + vertex_data[1]*vertex_data[1] + vertex_data[2]*vertex_data[2]);
		vertex_data[0] = vertex_data[0]/dist * depth;
		vertex_data[1] = vertex_data[1]/dist * depth;
		vertex_data[2] = vertex_data[2]/dist * depth;
	}
}

static void vertices_rotate(float *vertices, size_t num_vertices, float angle, glm::vec3 rotation_axis) {
	for(size_t i = 0; i < num_vertices - 2; i += 3) {
		float *vertex_data1 = &vertices[(i + 0) * 5];
		float *vertex_data2 = &vertices[(i + 1) * 5];
		float *vertex_data3 = &vertices[(i + 2) * 5];
		glm::quat quatRot = glm::angleAxis(angle, rotation_axis);
		glm::mat4x4 matRot = glm::mat4_cast(quatRot);
		glm::vec3 &vec1 = *(glm::vec3*)vertex_data1;
		glm::vec3 &vec2 = *(glm::vec3*)vertex_data2;
		glm::vec3 &vec3 = *(glm::vec3*)vertex_data3;
		glm::vec3 center = vertex_get_center(glm::mat3(vec1, vec2, vec3));

		vec1 -= center;
		vec2 -= center;
		vec3 -= center;

		glm::mat4 tran = glm::translate(matRot, center);

		glm::vec4 out1 = tran * glm::vec4(vec1.x, vec1.y, vec1.z, 1.0f);
		glm::vec4 out2 = tran * glm::vec4(vec2.x, vec2.y, vec2.z, 1.0f);
		glm::vec4 out3 = tran * glm::vec4(vec3.x, vec3.y, vec3.z, 1.0f);
		vec1 = glm::vec3(out1.x, out1.y, out1.z);
		vec2 = glm::vec3(out2.x, out2.y, out2.z);
		vec3 = glm::vec3(out3.x, out3.y, out3.z);
	}
}


//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
void CMainApplication::AddCubeToScene( const glm::mat4 &mat, std::vector<float> &vertdata )
{
	double width_ratio = (double)pixmap_texture_width / (double)pixmap_texture_height;
	arrow_ratio = width_ratio;

	int x_return, y_return;
	unsigned int width_return, height_return, border_width_return = 0, depth_return;
	
	if(projection_mode == ProjectionMode::SPHERE)
	{
		long columns = 32;
		long rows = 32;
		double angle_x = 3.14;
		double angle_y = 3.14;
		double radius_depth = 1.0;
		double radius_height = 0.5;
		double radius = radius_height * width_ratio * 0.5;

		for(long row = 0; row < rows; ++row) {
			for(long column = 0; column < columns; ++column) {
				double offset_angle = 0.0;//angle_x*0.5;

				double y_sin1 = sin((double)row / (double)rows * 3.14);
				double y_sin2 = sin((double)(row + 1) / (double)rows * 3.14);

				double z1 = sin(offset_angle + (double)column / (double)columns * angle_x) * radius;
				double z2 = sin(offset_angle + (double)(column + 1) / (double)columns * angle_x) * radius;
				double z3 = z1;

				double z4 = z3;
				double z5 = z2;
				double z6 = z2;

				z1 *= y_sin1;
				z2 *= y_sin1;
				z3 *= y_sin2;
				z4 *= y_sin2;
				z5 *= y_sin2;
				z6 *= y_sin1;

				double x1 = -cos(offset_angle + (double)column / (double)columns * angle_x) * radius;
				double x2 = -cos(offset_angle + (double)(column + 1) / (double)columns * angle_x) * radius;
				double x3 = x1;

				double x4 = x3;
				double x5 = x2;
				double x6 = x2;

				x1 *= y_sin1;
				x2 *= y_sin1;
				x3 *= y_sin2;
				x4 *= y_sin2;
				x5 *= y_sin2;
				x6 *= y_sin1;
	#if 0
				double y1 = cos((double)row / (double)rows * angle_y) * radius;
				double y2 = y1;
				double y3 = cos((double)(row + 1) / (double)rows * angle_y) * radius;

				double y4 = y3;
				double y5 = y3;
				double y6 = y1;

				z1 *= sin((double)row / (double)rows * angle_y) * radius_depth;
				z2 *= sin((double)row / (double)rows * angle_y) * radius_depth;
				z3 *= sin((double)(row + 1) / (double)rows * angle_y) * radius_depth;
				z4 *= sin((double)(row + 1) / (double)rows * angle_y) * radius_depth;
				z5 *= sin((double)(row + 1) / (double)rows * angle_y) * radius_depth;
				z6 *= sin((double)row / (double)rows * angle_y) * radius_depth;

				x1 *= sin((double)row / (double)rows * angle_y) * radius_depth;
				x2 *= sin((double)row / (double)rows * angle_y) * radius_depth;
				x3 *= sin((double)(row + 1) / (double)rows * angle_y) * radius_depth;
				x4 *= sin((double)(row + 1) / (double)rows * angle_y) * radius_depth;
				x5 *= sin((double)(row + 1) / (double)rows * angle_y) * radius_depth;
				x6 *= sin((double)row / (double)rows * angle_y) * radius_depth;
	#else
				double y1 = cos((double)row / (double)rows * 3.14) * radius_height;
				double y2 = y1;
				double y3 = cos((double)(row + 1) / (double)rows * 3.14) * radius_height;

				double y4 = y3;
				double y5 = y3;
				double y6 = y1;
	#endif

				glm::vec4 v1 = mat * glm::vec4(x1, y1, z1 + zoom, 1.0);
				glm::vec4 v2 = mat * glm::vec4(x2, y2, z2 + zoom, 1.0);
				glm::vec4 v3 = mat * glm::vec4(x3, y3, z3 + zoom, 1.0);
				glm::vec4 v4 = mat * glm::vec4(x4, y4, z4 + zoom, 1.0);
				glm::vec4 v5 = mat * glm::vec4(x5, y5, z5 + zoom, 1.0);
				glm::vec4 v6 = mat * glm::vec4(x6, y6, z6 + zoom, 1.0);

				AddCubeVertex(v1.x, v1.y, v1.z, 1.0 - (double)column / (double)columns,                 (double)row / (double)rows, vertdata);
				AddCubeVertex(v2.x, v2.y, v2.z, 1.0 - (double)(column + 1) / (double)columns,   (double)row / (double)rows, vertdata);
				AddCubeVertex(v3.x, v3.y, v3.z, 1.0 - (double)column / (double)columns,                 (double)(row + 1) / (double)rows, vertdata);

				AddCubeVertex(v4.x, v4.y, v4.z, 1.0 - (double)column / (double)columns,                 (double)(row + 1) / (double)rows, vertdata);
				AddCubeVertex(v5.x, v5.y, v5.z, 1.0 - (double)(column + 1) / (double)columns,   (double)(row + 1) / (double)rows, vertdata);
				AddCubeVertex(v6.x, v6.y, v6.z, 1.0 - (double)(column + 1) / (double)columns,   (double)row / (double)rows, vertdata);
			}
		}
	}
	else if (projection_mode == ProjectionMode::CYLINDER)
	{
		long columns = 64;
		double angle_start = -0.8;
		double angle_end = 0.8;
		double height = 1.5;
		double angle_len = angle_end - angle_start;

		double width_start = sin(angle_start);
		double width_end = sin(angle_start + angle_len);
		double target_radius = height * width_ratio;
		double radius = 2.0 * (target_radius / angle_len);

		for(long column = 0; column < columns; ++column) {
			double t1 = ((double)column / (double)columns);
			double t2 = (((double)column + 1) / (double)columns);

			double x1 = sin(angle_start + t1 * angle_len) * radius;
			double y1 = cos(angle_start + t1 * angle_len) * radius * 0.6;
			double x2 = sin(angle_start + t2 * angle_len) * radius;
			double y2 = cos(angle_start + t2 * angle_len) * radius * 0.6;

			//     2     n
			// 1  /|   / |    m
			// | / | /   |  / |
			// |/  2     n/   |
			// 1              m

			AddCubeVertex(x1, height, zoom + y1, 1 - t1, 0, vertdata);
			AddCubeVertex(x2, height, zoom + y2, 1 - t2, 0, vertdata);
			AddCubeVertex(x1, -height, zoom + y1, 1 - t1, 1, vertdata);

			AddCubeVertex(x1, -height, zoom + y1, 1 - t1, 1, vertdata);
			AddCubeVertex(x2, height, zoom + y2, 1 - t2, 0, vertdata);
			AddCubeVertex(x2, -height, zoom + y2, 1 - t2, 1, vertdata);
		}
	} else if (projection_mode == ProjectionMode::FLAT) {
		double height = 0.5;
		double width = height * (stretch ? 1.0 : 0.5) * width_ratio;
		AddCubeVertex(-width, 	 height, zoom, 1.0, 0.0, vertdata);
		AddCubeVertex(width, 	 height, zoom, 0.0, 0.0, vertdata);
		AddCubeVertex(-width, 	-height, zoom, 1.0, 1.0, vertdata);

		AddCubeVertex(-width, 	-height, zoom, 1.0, 1.0, vertdata);
		AddCubeVertex(width, 	-height, zoom, 0.0, 1.0, vertdata);
		AddCubeVertex(width, 	 height, zoom, 0.0, 0.0, vertdata);

		if(stretch)
			arrow_ratio = width_ratio * 2.0;
	} else if (projection_mode == ProjectionMode::SPHERE360) {
		border_width_return += 2; // Meh, hac k to deal with seams a bit
		double px = (double)border_width_return / (double)pixmap_texture_width;
		double py = (double)border_width_return / (double)pixmap_texture_height;

		double width = 1.0 - px * 2.0;
		double height = 1.0 - py * 2.0;

		double hz = zoom / (double)pixmap_texture_height;

		double texture_width = width / 3.0;
		double texture_height = height * 0.5;

		for(int i = 0; i < 3; ++i) {
			size_t plane_vertices_start = vertdata.size();
			CreateSegmentedPlane(vertdata, 1.0f, 1.0f, 1.0f, texture_width, texture_height - hz, texture_width * (2 - i) + px, py + hz, 32, 32);
			size_t plane_vertices_end = vertdata.size();
			size_t num_vertex_data = (plane_vertices_end - plane_vertices_start) / 5;

			plane_normalize_depth(&vertdata[plane_vertices_start], num_vertex_data, 1.0f);
			vertices_rotate(&vertdata[plane_vertices_start], num_vertex_data, -glm::half_pi<float>() + i * glm::half_pi<float>(), glm::vec3(0.0f, 1.0f, 0.0f));
		}

		for(int i = 0; i < 3; ++i) {
			size_t plane_vertices_start = vertdata.size();
			CreateSegmentedPlane(vertdata, 1.0f, 1.0f, 1.0f, texture_width, texture_height - hz, px + texture_width * i, 0.5f, 32, 32);
			size_t plane_vertices_end = vertdata.size();
			size_t num_vertex_data = (plane_vertices_end - plane_vertices_start) / 5;

			plane_normalize_depth(&vertdata[plane_vertices_start], num_vertex_data, 1.0f);
			vertices_rotate(&vertdata[plane_vertices_start], num_vertex_data, -glm::half_pi<float>(), glm::vec3(0.0f, 0.0f, 1.0f));
			vertices_rotate(&vertdata[plane_vertices_start], num_vertex_data, -glm::half_pi<float>() - i * glm::half_pi<float>(), glm::vec3(1.0f, 0.0f, 0.0f));
		}
	}
}


//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
void CMainApplication::SetupCompanionWindow()
{

	std::vector<VertexDataWindow> vVerts;

	// left eye verts
	vVerts.push_back( VertexDataWindow( glm::vec2(-1, -1), glm::vec2(0, 1)) );
	vVerts.push_back( VertexDataWindow( glm::vec2(0, -1), glm::vec2(1, 1)) );
	vVerts.push_back( VertexDataWindow( glm::vec2(-1, 1), glm::vec2(0, 0)) );
	vVerts.push_back( VertexDataWindow( glm::vec2(0, 1), glm::vec2(1, 0)) );

	// right eye verts
	vVerts.push_back( VertexDataWindow( glm::vec2(0, -1), glm::vec2(0, 1)) );
	vVerts.push_back( VertexDataWindow( glm::vec2(1, -1), glm::vec2(1, 1)) );
	vVerts.push_back( VertexDataWindow( glm::vec2(0, 1), glm::vec2(0, 0)) );
	vVerts.push_back( VertexDataWindow( glm::vec2(1, 1), glm::vec2(1, 0)) );

	GLushort vIndices[] = { 0, 1, 3,   0, 3, 2,   4, 5, 7,   4, 7, 6 };
	m_uiCompanionWindowIndexSize = _countof(vIndices);

	glGenVertexArrays( 1, &m_unCompanionWindowVAO );
	glBindVertexArray( m_unCompanionWindowVAO );

	glGenBuffers( 1, &m_glCompanionWindowIDVertBuffer );
	glBindBuffer( GL_ARRAY_BUFFER, m_glCompanionWindowIDVertBuffer );
	glBufferData( GL_ARRAY_BUFFER, vVerts.size()*sizeof(VertexDataWindow), &vVerts[0], GL_STATIC_DRAW );

	glGenBuffers( 1, &m_glCompanionWindowIDIndexBuffer );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_glCompanionWindowIDIndexBuffer );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, m_uiCompanionWindowIndexSize*sizeof(GLushort), &vIndices[0], GL_STATIC_DRAW );

	glEnableVertexAttribArray( 0 );
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(VertexDataWindow), (void *)offsetof( VertexDataWindow, position ) );

	glEnableVertexAttribArray( 1 );
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(VertexDataWindow), (void *)offsetof( VertexDataWindow, texCoord ) );

	glBindVertexArray( 0 );

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}


//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------

void
CMainApplication::RenderFrameView(
	int view_index,
	XrSpaceLocation* hand_locations,
	glm::mat4 projectionmatrix,
	glm::mat4 viewmatrix,
	FramebufferDesc* desc
){
	gl_egl_device_enter_context(&egl_data);
	glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
	glEnable( GL_MULTISAMPLE );

	glBindFramebuffer( GL_FRAMEBUFFER, desc->m_nRenderFramebufferId );
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, desc->m_nRenderTextureId, 0);
 	glViewport(0, 0, m_nRenderWidth, m_nRenderHeight );
 	RenderScene( view_index, &projectionmatrix, &viewmatrix);
	
 	glBindFramebuffer( GL_FRAMEBUFFER, 0 );
	
	glDisable( GL_MULTISAMPLE );
	 	
 	glBindFramebuffer(GL_READ_FRAMEBUFFER, desc->m_nRenderFramebufferId);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, desc->m_nResolveFramebufferId );

    glBlitFramebuffer( 0, 0, m_nRenderWidth, m_nRenderHeight, 0, 0, m_nRenderWidth, m_nRenderHeight, GL_COLOR_BUFFER_BIT, GL_LINEAR );

 	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	gl_egl_device_leave_context(&egl_data);
}


//-----------------------------------------------------------------------------
// Purpose: Renders a scene with respect to nEye.
//-----------------------------------------------------------------------------
void CMainApplication::RenderScene(
	int view_index,
	glm::mat4* projectionmatrix,
	glm::mat4* viewmatrix
)
{
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if(!got_texture)
		return;
	glEnable(GL_DEPTH_TEST);

	glUseProgram( m_unSceneProgramID );
	glUniformMatrix4fv( m_nSceneMatrixLocation, 1, GL_FALSE, glm::value_ptr(*viewmatrix));

	screencast_portal_cursor * cursor = NULL;
	if (content_mode == ContentMode::PIPEWIRE)
		cursor = screencast_portal_capture_get_cursor_data(window_texture);
	float m[2];
	if (cursor) {
		m[0] = cursor->x / (float)window_width;
		m[1] = cursor->y / (float)window_height;
	
		if(view_mode != ViewMode::PLANE) {
			if(cursor_wrap && m[0] >= 0.5f)
				m[0] -= 0.5f;
			else if(!cursor_wrap)
				m[0] *= 0.5f;
		}
	}

	if( view_index == 0 )
	{
		float offset = 0.0f;
		float scale = 0.5f;
		if(view_mode == ViewMode::RIGHT_LEFT) {
			offset = 0.5f;
		} else if(view_mode == ViewMode::PLANE || view_mode == ViewMode::SPHERE360) {
			offset = 0.0f;
			scale = 1.0f;
		}
		glUniform1fv(m_nSceneTextureOffsetXLocation, 1, &offset);
		glUniform1fv(m_nSceneTextureScaleXLocation, 1, &scale);

		if(view_mode == ViewMode::RIGHT_LEFT && cursor)
			m[0] += offset;
	}
	else if( view_index == 1 )
	{
		float offset = 0.5f;
		float scale = 0.5f;
		if (view_mode == ViewMode::RIGHT_LEFT) {
			offset = 0.0f;
		} else if (view_mode == ViewMode::PLANE || view_mode == ViewMode::SPHERE360) {
			offset = 0.0f;
			scale = 1.0f;
		}
		glUniform1fv(m_nSceneTextureOffsetXLocation, 1, &offset);
		glUniform1fv(m_nSceneTextureScaleXLocation, 1, &scale);

		if(view_mode == ViewMode::LEFT_RIGHT && cursor)
			m[0] += offset;
	}


	if (cursor && cursor->valid) {
		cursor_scale_uniform[0] = 0.01 * cursor_scale;
		cursor_scale_uniform[1] = cursor_scale_uniform[0] * arrow_ratio;

		float drawn_arrow_width = cursor_scale_uniform[0] * window_width;
		float drawn_arrow_height = cursor_scale_uniform[1] * window_height;
		float arrow_drawn_scale_x = drawn_arrow_width / (float)(cursor->width == 0 ? 1 : cursor->width);
		float arrow_drawn_scale_y = drawn_arrow_height / (float)(cursor->height == 0 ? 1 : cursor->height);

		m[0] += (-cursor->hotspot_x * arrow_drawn_scale_x) / (float)window_width;
		m[1] += (-cursor->hotspot_y * arrow_drawn_scale_y) / (float)window_height;
	} else {
		cursor_scale_uniform[0] = 0;
		cursor_scale_uniform[1] = 0;
	}

	glUniform2fv(m_nCursorLocation, 1, &m[0]);
	glUniform2fv(m_nArrowSizeLocation, 1, &cursor_scale_uniform[0]);

	glBindVertexArray( m_unSceneVAO );
	glActiveTexture(GL_TEXTURE0);
	switch (content_mode) {
		case ContentMode::PIPEWIRE:
			glBindTexture( GL_TEXTURE_2D, screencast_portal_capture_get_texture(window_texture) );
			break;
		case ContentMode::MPV:
			glBindTexture( GL_TEXTURE_2D, mpv.texture_id_resolved);
			break;
	}
	glActiveTexture(GL_TEXTURE1);
	// glBindTexture( GL_TEXTURE_2D, window_texture_get_opengl_texture_id(&window_texture) );
	glBindTexture(GL_TEXTURE_2D, (cursor && cursor->valid) ? cursor->texture: 0);
	glDrawArrays( GL_TRIANGLES, 0, m_uiVertcount );
	glBindVertexArray( 0 );
	glActiveTexture(GL_TEXTURE0);
	glUseProgram( 0 );
}


//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
void CMainApplication::RenderCompanionWindow()
{
	glDisable(GL_DEPTH_TEST);
	glViewport( 0, 0, m_nCompanionWindowWidth, m_nCompanionWindowHeight );

	glBindVertexArray( m_unCompanionWindowVAO );
	glUseProgram( m_unCompanionWindowProgramID );

	// render left eye (first half of index array )
	glBindTexture(GL_TEXTURE_2D, leftEyeDesc.m_nResolveTextureId );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glDrawElements( GL_TRIANGLES, m_uiCompanionWindowIndexSize/2, GL_UNSIGNED_SHORT, 0 );

	// render right eye (second half of index array )
	glBindTexture(GL_TEXTURE_2D, rightEyeDesc.m_nResolveTextureId  );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glDrawElements( GL_TRIANGLES, m_uiCompanionWindowIndexSize/2, GL_UNSIGNED_SHORT, (const void *)(uintptr_t)(m_uiCompanionWindowIndexSize) );

	glBindVertexArray( 0 );
	glUseProgram( 0 );
}


CMainApplication *pMainApplication;

void reset_position(int signum)
{
	printf("ok\n");
	pMainApplication->ResetRotation();
}

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	// Force SDL_VIDEODRIVER=wayland
	setenv("SDL_VIDEODRIVER", "wayland", true);
	
	pMainApplication = new CMainApplication( argc, argv );

	signal(SIGUSR1, reset_position);
	signal(SIGUSR2, reset_position);

	if (!pMainApplication->BInit())
	{
		pMainApplication->Shutdown();
		return 1;
	}
	
	fflush(stdout);
	pMainApplication->RunMainLoop();

	pMainApplication->Shutdown();

	return pMainApplication->exit_code;
}
