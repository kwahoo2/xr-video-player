/* pipewire.c
 *
 * Copyright 2020 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "pipewire.h"

// #include <util/darray.h>

#include <gio/gio.h>
#include <gio/gunixfdlist.h>

#include <fcntl.h>
// #include <glad/glad.h>
#include <linux/dma-buf.h>
#include <libdrm/drm_fourcc.h>
#include <pipewire/pipewire.h>
#include <spa/param/video/format-utils.h>
#include <spa/debug/format.h>
#include <spa/debug/types.h>
#include <spa/param/video/type-info.h>
#include <spa/utils/result.h>
#include <glib/gstrfuncs.h>
#include <X11/Xlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <GL/glx.h>

#ifndef SPA_POD_PROP_FLAG_DONT_FIXATE
#define SPA_POD_PROP_FLAG_DONT_FIXATE (1 << 4)
#endif

#define CURSOR_META_SIZE(width, height)                                    \
	(sizeof(struct spa_meta_cursor) + sizeof(struct spa_meta_bitmap) + \
	 width * height * 4)

struct pw_data_version {
	int major;
	int minor;
	int micro;
};

struct video_info {
	uint32_t fps_num;
	uint32_t fps_den;
};

struct format_info {
	uint32_t spa_format;
	uint32_t drm_format;
	struct {
		uint32_t num;
		uint64_t array[1000];
	} modifiers;
};

struct _pipewire_data {
	uint32_t pipewire_node;
	int pipewire_fd;

	struct wl_display * display;
	struct gl_egl_data egl_data;
	GLuint texture;

	struct pw_thread_loop *thread_loop;
	struct pw_context *context;

	struct pw_core *core;
	struct spa_hook core_listener;
	int server_version_sync;

	struct pw_data_version server_version;

	struct pw_stream *stream;
	struct spa_hook stream_listener;
	struct spa_source *reneg;

	struct spa_video_info format;

	struct {
		bool valid;
		int x, y;
		uint32_t width, height;
	} crop;

	struct {
		bool visible;
		bool valid;
		int x, y;
		int hotspot_x, hotspot_y;
		int width, height;
		GLuint texture;
	} cursor;

	struct video_info video_info;
	bool negotiated;

	struct {
		uint32_t num;
		struct format_info array[10];
	} format_info;
};


static const int ctx_attribs[] = {
#ifdef _DEBUG
	EGL_CONTEXT_OPENGL_DEBUG,
	EGL_TRUE,
#endif
	EGL_CONTEXT_OPENGL_PROFILE_MASK,
	EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT,
	EGL_CONTEXT_MAJOR_VERSION,
	3,
	EGL_CONTEXT_MINOR_VERSION,
	3,
	EGL_NONE,
};

static const EGLint ctx_config_attribs[] = {
	EGL_STENCIL_SIZE,
	0,
	EGL_DEPTH_SIZE,
	0,
	EGL_BUFFER_SIZE,
	32,
	EGL_ALPHA_SIZE,
	8,
	EGL_RENDERABLE_TYPE,
	EGL_OPENGL_BIT,
	EGL_SURFACE_TYPE,
	EGL_WINDOW_BIT | EGL_PBUFFER_BIT,
	EGL_NONE,
};

/* auxiliary methods */

static bool parse_pw_version(struct pw_data_version *dst, const char *version)
{
	int n_matches = sscanf(version, "%d.%d.%d", &dst->major, &dst->minor,
			       &dst->micro);
	return n_matches == 3;
}

static bool check_pw_version(const struct pw_data_version *pw_version, int major,
			     int minor, int micro)
{
	if (pw_version->major != major)
		return pw_version->major > major;
	if (pw_version->minor != minor)
		return pw_version->minor > minor;
	return pw_version->micro >= micro;
}

static void update_pw_versions(pipewire_data *pw_data, const char *version)
{
	printf("[INFO pipewire] Server version: %s\n", version);
	printf("[INFO pipewire] Library version: %s\n",
	     pw_get_library_version());
	printf("[INFO pipewire] Header version: %s\n",
	     pw_get_headers_version());

	if (!parse_pw_version(&pw_data->server_version, version))
		printf("[WARNING pipewire] failed to parse server version\n");
}

static void teardown_pipewire(pipewire_data *pw_data)
{
	if (pw_data->thread_loop) {
		pw_thread_loop_wait(pw_data->thread_loop);
		pw_thread_loop_stop(pw_data->thread_loop);
	}

	if (pw_data->stream)
		pw_stream_disconnect(pw_data->stream);
	g_clear_pointer(&pw_data->stream, pw_stream_destroy);
	g_clear_pointer(&pw_data->context, pw_context_destroy);
	g_clear_pointer(&pw_data->thread_loop, pw_thread_loop_destroy);

	if (pw_data->pipewire_fd > 0) {
		close(pw_data->pipewire_fd);
		pw_data->pipewire_fd = 0;
	}

	pw_data->negotiated = false;
}

static void destroy_session(pipewire_data *pw_data)
{
	gl_egl_device_enter_context(&pw_data->egl_data);
	if(pw_data->texture) {
		glDeleteTextures(1, &pw_data->texture);
		pw_data->texture = 0;
	}
	if(pw_data->cursor.texture) {
		glDeleteTextures(1, &pw_data->cursor.texture);
		pw_data->cursor.texture = 0;
	}
	gl_egl_device_leave_context(&pw_data->egl_data);
}

static inline bool has_effective_crop(pipewire_data *pw_data)
{
	return pw_data->crop.valid &&
	       (pw_data->crop.x != 0 || pw_data->crop.y != 0 ||
		pw_data->crop.width < pw_data->format.info.raw.size.width ||
		pw_data->crop.height < pw_data->format.info.raw.size.height);
}

static const struct {
	uint32_t spa_format;
	uint32_t drm_format;
	gs_color_format gs_format;
	bool swap_red_blue;
	const char *pretty_name;
} supported_formats[] = {
	{
		SPA_VIDEO_FORMAT_BGRA,
		DRM_FORMAT_ARGB8888,
		GL_BGRA,
		false,
		"ARGB8888",
	},
	{
		SPA_VIDEO_FORMAT_RGBA,
		DRM_FORMAT_ABGR8888,
		GL_RGBA,
		false,
		"ABGR8888",
	},
	{
		SPA_VIDEO_FORMAT_BGRx,
		DRM_FORMAT_XRGB8888,
		GL_BGRA,
		false,
		"XRGB8888",
	},
	{
		SPA_VIDEO_FORMAT_RGBx,
		DRM_FORMAT_XBGR8888,
		GL_BGRA,
		true,
		"XBGR8888",
	},
};

#define N_SUPPORTED_FORMATS \
	(sizeof(supported_formats) / sizeof(supported_formats[0]))

static bool lookup_format_info_from_spa_format(
	uint32_t spa_format, uint32_t *out_drm_format,
	gs_color_format *out_gs_format, bool *out_swap_red_blue)
{
	for (size_t i = 0; i < N_SUPPORTED_FORMATS; i++) {
		if (supported_formats[i].spa_format != spa_format)
			continue;

		if (out_drm_format)
			*out_drm_format = supported_formats[i].drm_format;
		if (out_gs_format)
			*out_gs_format = supported_formats[i].gs_format;
		if (out_swap_red_blue)
			*out_swap_red_blue = supported_formats[i].swap_red_blue;
		return true;
	}
	return false;
}

static void swap_texture_red_blue(GLuint texture)
{
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_B, GL_RED);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_R, GL_BLUE);
	glBindTexture(GL_TEXTURE_2D, 0);
}

static inline struct spa_pod *build_format(struct spa_pod_builder *b,
					   struct video_info *ovi,
					   uint32_t format, uint64_t *modifiers,
					   size_t modifier_count)
{
	struct spa_pod_frame format_frame;

	/* Make an object of type SPA_TYPE_OBJECT_Format and id SPA_PARAM_EnumFormat.
	 * The object type is important because it defines the properties that are
	 * acceptable. The id gives more context about what the object is meant to
	 * contain. In this case we enumerate supported formats. */
	spa_pod_builder_push_object(b, &format_frame, SPA_TYPE_OBJECT_Format,
				    SPA_PARAM_EnumFormat);
	/* add media type and media subtype properties */
	spa_pod_builder_add(b, SPA_FORMAT_mediaType,
			    SPA_POD_Id(SPA_MEDIA_TYPE_video), 0);
	spa_pod_builder_add(b, SPA_FORMAT_mediaSubtype,
			    SPA_POD_Id(SPA_MEDIA_SUBTYPE_raw), 0);

	/* formats */
	spa_pod_builder_add(b, SPA_FORMAT_VIDEO_format, SPA_POD_Id(format), 0);

	/* modifier */
	if (modifier_count > 0) {
		struct spa_pod_frame modifier_frame;

		/* build an enumeration of modifiers */
		spa_pod_builder_prop(b, SPA_FORMAT_VIDEO_modifier,
				     SPA_POD_PROP_FLAG_MANDATORY |
					     SPA_POD_PROP_FLAG_DONT_FIXATE);

		spa_pod_builder_push_choice(b, &modifier_frame, SPA_CHOICE_Enum,
					    0);

		/* The first element of choice pods is the preferred value. Here
		 * we arbitrarily pick the first modifier as the preferred one.
		 */
		spa_pod_builder_long(b, modifiers[0]);

		/* modifiers from  an array */
		for (uint32_t i = 0; i < modifier_count; i++)
			spa_pod_builder_long(b, modifiers[i]);

		spa_pod_builder_pop(b, &modifier_frame);
	}
	/* add size and framerate ranges */
	spa_pod_builder_add(b, SPA_FORMAT_VIDEO_size,
			    SPA_POD_CHOICE_RANGE_Rectangle(
				    &SPA_RECTANGLE(320, 240), // Arbitrary
				    &SPA_RECTANGLE(1, 1),
				    &SPA_RECTANGLE(8192, 4320)),
			    SPA_FORMAT_VIDEO_framerate,
			    SPA_POD_CHOICE_RANGE_Fraction(
				    &SPA_FRACTION(ovi->fps_num, ovi->fps_den),
				    &SPA_FRACTION(0, 1), &SPA_FRACTION(360, 1)),
			    0);
	return spa_pod_builder_pop(b, &format_frame);
}

static bool build_format_params(pipewire_data *pw_data,
				struct spa_pod_builder *pod_builder,
				const struct spa_pod ***param_list,
				uint32_t *n_params)
{
	uint32_t params_count = 0;

	const struct spa_pod **params;
	params =
		malloc(2 * pw_data->format_info.num * sizeof(struct spa_pod *));

	if (!params) {
		printf(
		     "[ERROR pipewire] Failed to allocate memory for param pointers");
		return false;
	}

	if (!check_pw_version(&pw_data->server_version, 0, 3, 33))
		goto build_shm;

	for (size_t i = 0; i < pw_data->format_info.num; i++) {
		if (pw_data->format_info.array[i].modifiers.num == 0) {
			continue;
		}
		params[params_count++] = build_format(
			pod_builder, &pw_data->video_info,
			pw_data->format_info.array[i].spa_format,
			pw_data->format_info.array[i].modifiers.array,
			pw_data->format_info.array[i].modifiers.num);
	}

build_shm:
	for (size_t i = 0; i < pw_data->format_info.num; i++) {
		params[params_count++] = build_format(
			pod_builder, &pw_data->video_info,
			pw_data->format_info.array[i].spa_format, NULL, 0);
	}
	*param_list = params;
	*n_params = params_count;
	return true;
}

static bool drm_format_available(uint32_t drm_format, uint32_t *drm_formats,
				 size_t n_drm_formats)
{
	for (size_t j = 0; j < n_drm_formats; j++) {
		if (drm_format == drm_formats[j]) {
			return true;
		}
	}
	return false;
}

static void init_format_info(pipewire_data *pw_data)
{
	pw_data->format_info.num = 0;
	gl_egl_device_enter_context(&pw_data->egl_data);

	enum gs_dmabuf_flags dmabuf_flags;
	uint32_t *drm_formats = NULL;
	size_t n_drm_formats;

	bool capabilities_queried = gl_egl_query_dmabuf_capabilities(
		pw_data->egl_data.display, &dmabuf_flags, &drm_formats, &n_drm_formats);

	for (size_t i = 0; i < N_SUPPORTED_FORMATS; i++) {
		struct format_info *info;

		if (!drm_format_available(supported_formats[i].drm_format,
					  drm_formats, n_drm_formats))
			continue;

		info = &pw_data->format_info.array[pw_data->format_info.num++];
		info->modifiers.num = 0;
		info->spa_format = supported_formats[i].spa_format;
		info->drm_format = supported_formats[i].drm_format;

		if (!capabilities_queried)
			continue;

		size_t n_modifiers;
		uint64_t *modifiers = NULL;
		if (gl_egl_query_dmabuf_modifiers_for_format(
			    pw_data->egl_data.display, supported_formats[i].drm_format, &modifiers,
			    &n_modifiers)) {
			for (size_t j = 0; j < n_modifiers; j++) {
				info->modifiers.array[info->modifiers.num++] = modifiers[j];
			}
		}
		free(modifiers);

		if (dmabuf_flags &
		    GS_DMABUF_FLAG_IMPLICIT_MODIFIERS_SUPPORTED) {
			info->modifiers.array[info->modifiers.num++] = DRM_FORMAT_MOD_INVALID;
		}
	}
	gl_egl_device_leave_context(&pw_data->egl_data);

	free(drm_formats);
}

static void clear_format_info(pipewire_data *pw_data)
{
	for (size_t i = 0; i < pw_data->format_info.num; i++) {
		pw_data->format_info.array[i].modifiers.num = 0;
	}
	pw_data->format_info.num = 0;
}

static void remove_modifier_from_format(pipewire_data *pw_data,
					uint32_t spa_format, uint64_t modifier)
{
	for (size_t i = 0; i < pw_data->format_info.num; i++) {
		if (pw_data->format_info.array[i].spa_format != spa_format)
			continue;

		if (!check_pw_version(&pw_data->server_version, 0, 3, 40)) {
			pw_data->format_info.array[i].modifiers.array[0] = pw_data->format_info.array[i].modifiers.array[pw_data->format_info.array[i].modifiers.num - 1];
			pw_data->format_info.array[i].modifiers.num = 1;
			continue;
		}

		int idx;
		while (true) {
			idx = -1;
			for (int j = 0; i < pw_data->format_info.array[i].modifiers.num; j++) {
				if (pw_data->format_info.array[i].modifiers.array[j] == modifier) {
					idx = j;
					break;
				}
			}
			if (idx == -1) break;
			pw_data->format_info.array[i].modifiers.array[idx] = pw_data->format_info.array[i].modifiers.array[(pw_data->format_info.array[i].modifiers.num--) - 1];
		}
	}
}

static void renegotiate_format(void *data, uint64_t expirations)
{
	UNUSED_PARAMETER(expirations);
	pipewire_data *pw_data = (pipewire_data *)data;
	const struct spa_pod **params = NULL;

	printf("[INFO pipewire] Renegotiating stream\n");

	pw_thread_loop_lock(pw_data->thread_loop);

	uint8_t params_buffer[2048];
	struct spa_pod_builder pod_builder =
		SPA_POD_BUILDER_INIT(params_buffer, sizeof(params_buffer));
	uint32_t n_params;
	if (!build_format_params(pw_data, &pod_builder, &params, &n_params)) {
		teardown_pipewire(pw_data);
		pw_thread_loop_unlock(pw_data->thread_loop);
		return;
	}

	pw_stream_update_params(pw_data->stream, params, n_params);
	pw_thread_loop_unlock(pw_data->thread_loop);
	free(params);
}

/* ------------------------------------------------- */

static void on_process_cb(void *user_data)
{
	pipewire_data *pw_data = user_data;
	struct spa_meta_cursor *cursor;
	uint32_t drm_format;
	struct spa_meta_region *region;
	struct spa_buffer *buffer;
	struct pw_buffer *b;
	bool swap_red_blue = false;
	bool has_buffer;

	/* Find the most recent buffer */
	b = NULL;
	while (true) {
		struct pw_buffer *aux =
			pw_stream_dequeue_buffer(pw_data->stream);
		if (!aux)
			break;
		if (b)
			pw_stream_queue_buffer(pw_data->stream, b);
		b = aux;
	}

	if (!b) {
		printf("[DEBUG pipewire] Out of buffers!\n");
		return;
	}

	buffer = b->buffer;
	has_buffer = buffer->datas[0].chunk->size != 0;

	if (!has_buffer)
		goto read_metadata;

	if (buffer->datas[0].type == SPA_DATA_DmaBuf) {
		uint32_t planes = buffer->n_datas;
		uint32_t offsets[planes];
		uint32_t strides[planes];
		uint64_t modifiers[planes];
		int fds[planes];
		bool use_modifiers;

		if (!lookup_format_info_from_spa_format(
			    pw_data->format.info.raw.format, &drm_format, NULL,
			    NULL)) {

			printf(
				"[DEBUG][pipewire] DMA-BUF info: fd:%ld, stride:%d, offset:%u, size:%dx%d\n",
				buffer->datas[0].fd, buffer->datas[0].chunk->stride,
				buffer->datas[0].chunk->offset,
				pw_data->format.info.raw.size.width,
				pw_data->format.info.raw.size.height);
			printf(
			     "[ERROR][pipewire] unsupported DMA buffer format: %d\n",
			     pw_data->format.info.raw.format);
			goto read_metadata;
		}

		for (uint32_t plane = 0; plane < planes; plane++) {
			fds[plane] = buffer->datas[plane].fd;
			offsets[plane] = buffer->datas[plane].chunk->offset;
			strides[plane] = buffer->datas[plane].chunk->stride;
			modifiers[plane] = pw_data->format.info.raw.modifier;
		}
		
		gl_egl_device_enter_context(&pw_data->egl_data);

		use_modifiers = pw_data->format.info.raw.modifier !=
				DRM_FORMAT_MOD_INVALID;
		pw_data->texture = gl_egl_create_dmabuf_image(
			pw_data->egl_data.display,
			pw_data->format.info.raw.size.width,
			pw_data->format.info.raw.size.height, drm_format,
			GL_BGRA, planes, fds, strides, offsets,
			use_modifiers ? modifiers : NULL,
			pw_data->texture
		);
		
		gl_egl_device_leave_context(&pw_data->egl_data);
		
		/*
		printf(
			"[DEBUG][pipewire] DMA-BUF info: fd:%ld, stride:%d, offset:%u, size:%dx%d, texture id:%d\n",
			buffer->datas[0].fd, buffer->datas[0].chunk->stride,
			buffer->datas[0].chunk->offset,
			pw_data->format.info.raw.size.width,
			pw_data->format.info.raw.size.height,
			pw_data->texture
		);
		*/

		if (pw_data->texture == 0) {
			/*
			remove_modifier_from_format(
				pw_data, pw_data->format.info.raw.format,
				pw_data->format.info.raw.modifier);
			pw_loop_signal_event(
				pw_thread_loop_get_loop(pw_data->thread_loop),
				pw_data->reneg);
			*/
		}
	} else {
		printf("[ERROR pipewire] Buffer has memory texture ----- not supported\n");
		gs_color_format gs_format;

		if (!lookup_format_info_from_spa_format(
			    pw_data->format.info.raw.format, NULL, &gs_format,
			    &swap_red_blue)) {
			printf(
			     "[ERROR][pipewire] unsupported DMA buffer format: %d\n",
			     pw_data->format.info.raw.format);
			goto read_metadata;
		}

		if(pw_data->texture) {
			glDeleteTextures(1, &pw_data->texture);
			pw_data->texture = 0;
		}
		/*
		pw_data->texture = gs_texture_create(
			pw_data->format.info.raw.size.width,
			pw_data->format.info.raw.size.height, gs_format, 1,
			(const uint8_t **)&buffer->datas[0].data, GS_DYNAMIC);
		*/
	}

	if (swap_red_blue)
		swap_texture_red_blue(pw_data->texture);

	/* Video Crop */
	region = spa_buffer_find_meta_data(buffer, SPA_META_VideoCrop,
					   sizeof(*region));
	if (region && spa_meta_region_is_valid(region)) {
		printf(
		     "[DEBUG][pipewire] Crop Region available (%dx%d+%d+%d)\n",
		     region->region.position.x, region->region.position.y,
		     region->region.size.width, region->region.size.height);

		pw_data->crop.x = region->region.position.x;
		pw_data->crop.y = region->region.position.y;
		pw_data->crop.width = region->region.size.width;
		pw_data->crop.height = region->region.size.height;
		pw_data->crop.valid = true;
	} else {
		pw_data->crop.valid = false;
	}

read_metadata:

	/* Cursor */
	cursor = spa_buffer_find_meta_data(buffer, SPA_META_Cursor,
					   sizeof(*cursor));
	pw_data->cursor.valid = cursor && spa_meta_cursor_is_valid(cursor);
	
	if (pw_data->cursor.visible && pw_data->cursor.valid) {
		struct spa_meta_bitmap *bitmap = NULL;
		gs_color_format gs_format;

		if (cursor->bitmap_offset)
			bitmap = SPA_MEMBER(cursor, cursor->bitmap_offset,
					    struct spa_meta_bitmap);

		if (bitmap && bitmap->size.width > 0 &&
		    bitmap->size.height > 0 &&
		    lookup_format_info_from_spa_format(
			    bitmap->format, NULL, &gs_format, &swap_red_blue)) {
			const uint8_t *bitmap_data;

			bitmap_data =
				SPA_MEMBER(bitmap, bitmap->offset, uint8_t);
		
			gl_egl_device_enter_context(&pw_data->egl_data);
			
			// If something changes, a new texture might be needed
			if (pw_data->cursor.texture 
				&& (
					false
					|| pw_data->cursor.hotspot_x != cursor->hotspot.x
					|| pw_data->cursor.hotspot_y != cursor->hotspot.y
					|| pw_data->cursor.width != bitmap->size.width
					|| pw_data->cursor.height != bitmap->size.height
				)
			) {
				glDeleteTextures(1, &pw_data->cursor.texture);
				pw_data->cursor.texture = 0;
			}
			pw_data->cursor.hotspot_x = cursor->hotspot.x;
			pw_data->cursor.hotspot_y = cursor->hotspot.y;
			pw_data->cursor.width = bitmap->size.width;
			pw_data->cursor.height = bitmap->size.height;

			pw_data->cursor.texture = gl_egl_create_texture_from_bitmap(
				pw_data->egl_data.display,
				pw_data->cursor.width,
				pw_data->cursor.height, 
				gs_format,
				GL_TEXTURE_2D,
				&bitmap_data,
				pw_data->cursor.texture
			);
			
			gl_egl_device_leave_context(&pw_data->egl_data);
		}

		pw_data->cursor.x = cursor->position.x;
		pw_data->cursor.y = cursor->position.y;
		// printf("[INFO] Cursor: %d %d %d %d\n", cursor->hotspot.x, cursor->hotspot.y, cursor->position.x, cursor->position.y);
	}

	pw_stream_queue_buffer(pw_data->stream, b);
}

static void on_param_changed_cb(void *user_data, uint32_t id,
				const struct spa_pod *param)
{
	pipewire_data *pw_data = user_data;
	struct spa_pod_builder pod_builder;
	const struct spa_pod *params[3];
	uint32_t buffer_types;
	uint8_t params_buffer[1024];
	int result;

	if (!param || id != SPA_PARAM_Format)
		return;

	result = spa_format_parse(param, &pw_data->format.media_type,
				  &pw_data->format.media_subtype);
	if (result < 0)
		return;

	if (pw_data->format.media_type != SPA_MEDIA_TYPE_video ||
	    pw_data->format.media_subtype != SPA_MEDIA_SUBTYPE_raw)
		return;

	spa_format_video_raw_parse(param, &pw_data->format.info.raw);

	buffer_types = 1 << SPA_DATA_MemPtr;
	bool has_modifier =
		spa_pod_find_prop(param, NULL, SPA_FORMAT_VIDEO_modifier) !=
		NULL;
	if (has_modifier || check_pw_version(&pw_data->server_version, 0, 3, 24))
		buffer_types |= 1 << SPA_DATA_DmaBuf;

	printf("[INFO pipewire] Negotiated format:\n");

	printf("[INFO pipewire]     Format: %d (%s)\n",
	     pw_data->format.info.raw.format,
	     spa_debug_type_find_name(spa_type_video_format,
				      pw_data->format.info.raw.format));

	if (has_modifier) {
		printf("[INFO pipewire]     Modifier: %" PRIu64 "\n",
		     pw_data->format.info.raw.modifier);
	}

	printf("[INFO pipewire]     Size: %dx%d\n",
	     pw_data->format.info.raw.size.width,
	     pw_data->format.info.raw.size.height);

	printf("[INFO pipewire]     Framerate: %d/%d\n",
	     pw_data->format.info.raw.framerate.num,
	     pw_data->format.info.raw.framerate.denom);

	/* Video crop */
	pod_builder =
		SPA_POD_BUILDER_INIT(params_buffer, sizeof(params_buffer));
	params[0] = spa_pod_builder_add_object(
		&pod_builder, SPA_TYPE_OBJECT_ParamMeta, SPA_PARAM_Meta,
		SPA_PARAM_META_type, SPA_POD_Id(SPA_META_VideoCrop),
		SPA_PARAM_META_size,
		SPA_POD_Int(sizeof(struct spa_meta_region)));

	/* Cursor */
	params[1] = spa_pod_builder_add_object(
		&pod_builder, SPA_TYPE_OBJECT_ParamMeta, SPA_PARAM_Meta,
		SPA_PARAM_META_type, SPA_POD_Id(SPA_META_Cursor),
		SPA_PARAM_META_size,
		SPA_POD_CHOICE_RANGE_Int(CURSOR_META_SIZE(64, 64),
					 CURSOR_META_SIZE(1, 1),
					 CURSOR_META_SIZE(1024, 1024)));

	/* Buffer options */
	params[2] = spa_pod_builder_add_object(
		&pod_builder, SPA_TYPE_OBJECT_ParamBuffers, SPA_PARAM_Buffers,
		SPA_PARAM_BUFFERS_dataType, SPA_POD_Int(buffer_types));
	
	// Delete texture when renegotiating
	gl_egl_device_enter_context(&pw_data->egl_data);
	if(pw_data->texture) {
		glDeleteTextures(1, &pw_data->texture);
		pw_data->texture = 0;
	}
	gl_egl_device_leave_context(&pw_data->egl_data);

	pw_stream_update_params(pw_data->stream, params, 3);

	pw_data->negotiated = true;
}

static void on_state_changed_cb(void *user_data, enum pw_stream_state old,
				enum pw_stream_state state, const char *error)
{
	UNUSED_PARAMETER(old);

	pipewire_data *pw_data = user_data;

	printf("[INFO pipewire] Stream %p state: \"%s\" (error: %s)\n",
	     pw_data->stream, pw_stream_state_as_string(state),
	     error ? error : "none");
}

static const struct pw_stream_events stream_events = {
	PW_VERSION_STREAM_EVENTS,
	.state_changed = on_state_changed_cb,
	.param_changed = on_param_changed_cb,
	.process = on_process_cb,
};

static void on_core_info_cb(void *user_data, const struct pw_core_info *info)
{
	pipewire_data *pw_data = user_data;

	update_pw_versions(pw_data, info->version);
}

static void on_core_error_cb(void *user_data, uint32_t id, int seq, int res,
			     const char *message)
{
	pipewire_data *pw_data = user_data;

	printf("[ERROR pipewire] Error id:%u seq:%d res:%d: %s\n", id,
	     seq, res, message);

	pw_thread_loop_signal(pw_data->thread_loop, FALSE);
}

static void on_core_done_cb(void *user_data, uint32_t id, int seq)
{
	pipewire_data *pw_data = user_data;

	if (id == PW_ID_CORE && pw_data->server_version_sync == seq)
		pw_thread_loop_signal(pw_data->thread_loop, FALSE);
}

static const struct pw_core_events core_events = {
	PW_VERSION_CORE_EVENTS,
	.info = on_core_info_cb,
	.done = on_core_done_cb,
	.error = on_core_error_cb,
};

static void play_pipewire_stream(pipewire_data *pw_data)
{
	struct spa_pod_builder pod_builder;
	const struct spa_pod **params = NULL;
	uint32_t n_params;
	uint8_t params_buffer[2048];

	pw_data->thread_loop = pw_thread_loop_new("PipeWire thread loop", NULL);
	pw_data->context = pw_context_new(
		pw_thread_loop_get_loop(pw_data->thread_loop), NULL, 0);

	if (pw_thread_loop_start(pw_data->thread_loop) < 0) {
		printf("[WARNING]Error starting threaded mainloop\n");
		return;
	}

	pw_thread_loop_lock(pw_data->thread_loop);

	/* Core */
	pw_data->core = pw_context_connect_fd(
		pw_data->context, fcntl(pw_data->pipewire_fd, F_DUPFD_CLOEXEC, 5),
		NULL, 0);
	if (!pw_data->core) {
		printf("[WARNING]Error creating PipeWire core: %m\n");
		pw_thread_loop_unlock(pw_data->thread_loop);
		return;
	}

	pw_core_add_listener(pw_data->core, &pw_data->core_listener, &core_events,
			     pw_data);

	/* Signal to renegotiate */
	pw_data->reneg =
		pw_loop_add_event(pw_thread_loop_get_loop(pw_data->thread_loop),
				  renegotiate_format, pw_data);
	printf("[DEBUG pipewire] registered event %p\n", pw_data->reneg);

	// Dispatch to receive the info core event
	pw_data->server_version_sync = pw_core_sync(pw_data->core, PW_ID_CORE,
						   pw_data->server_version_sync);
	pw_thread_loop_wait(pw_data->thread_loop);

	/* Stream */
	pw_data->stream = pw_stream_new(
		pw_data->core, "xr-video-player",
		pw_properties_new(PW_KEY_MEDIA_TYPE, "Video",
				  PW_KEY_MEDIA_CATEGORY, "Capture",
				  PW_KEY_MEDIA_ROLE, "Screen", NULL));
	pw_stream_add_listener(pw_data->stream, &pw_data->stream_listener,
			       &stream_events, pw_data);
	printf("[INFO pipewire] Created stream %p\n", pw_data->stream);

	/* Stream parameters */
	pod_builder =
		SPA_POD_BUILDER_INIT(params_buffer, sizeof(params_buffer));

	pw_data->video_info.fps_num = 60;
	pw_data->video_info.fps_den = 1;

	if (!build_format_params(pw_data, &pod_builder, &params, &n_params)) {
		pw_thread_loop_unlock(pw_data->thread_loop);
		teardown_pipewire(pw_data);
		return;
	}

	pw_stream_connect(
		pw_data->stream, PW_DIRECTION_INPUT, pw_data->pipewire_node,
		PW_STREAM_FLAG_AUTOCONNECT | PW_STREAM_FLAG_MAP_BUFFERS, params,
		n_params);

	printf("[INFO pipewire] Playing stream %p\n", pw_data->stream);
	pw_thread_loop_unlock(pw_data->thread_loop);
	free(params);
}

void *pipewire_create(int pipewire_fd, int pipewire_node, struct gl_egl_data *egl_data)
{
	pipewire_data *pw_data = malloc(sizeof(pipewire_data));

	pw_data->pipewire_fd = pipewire_fd;
	pw_data->pipewire_node = pipewire_node;
	pw_data->egl_data.mutex = egl_data->mutex;
	pw_data->egl_data.mutex_counter = egl_data->mutex_counter;

	gl_egl_device_enter_context(egl_data);
	gl_egl_context_create(&pw_data->egl_data, ctx_config_attribs, ctx_attribs);
	gl_egl_device_leave_context(egl_data);
	
	gl_egl_device_enter_context(&pw_data->egl_data);
	GLuint texture = 0;
	glGenTextures(1, &texture);
	if (texture == 0) {
		printf("[ERROR] Start Failed get new texture\n");
		return 0;
	}
	glDeleteTextures(1, &texture);
	gl_egl_device_leave_context(&pw_data->egl_data);
	
	init_format_info(pw_data);
	play_pipewire_stream(pw_data);

	return pw_data;
}

void pipewire_destroy(pipewire_data *pw_data)
{
	if (!pw_data)
		return;

	gl_egl_context_destroy(&pw_data->egl_data);
	teardown_pipewire(pw_data);
	destroy_session(pw_data);

	clear_format_info(pw_data);

	free(pw_data);
}

void pipewire_show(pipewire_data *pw_data)
{
	if (pw_data->stream)
		pw_stream_set_active(pw_data->stream, true);
}

void pipewire_hide(pipewire_data *pw_data)
{
	if (pw_data->stream)
		pw_stream_set_active(pw_data->stream, false);
}

uint32_t pipewire_get_width(pipewire_data *pw_data)
{
	if (!pw_data->negotiated)
		return 0;

	if (pw_data->crop.valid)
		return pw_data->crop.width;
	else
		return pw_data->format.info.raw.size.width;
}

uint32_t pipewire_get_height(pipewire_data *pw_data)
{
	if (!pw_data->negotiated)
		return 0;

	if (pw_data->crop.valid)
		return pw_data->crop.height;
	else
		return pw_data->format.info.raw.size.height;
}

GLuint pipewire_get_texture(pipewire_data *pw_data)
{
	if (!pw_data->negotiated)
		return 0;
	return pw_data->texture;
}

void *pipewire_get_cursor_data(pipewire_data *pw_data)
{
	if (!pw_data->negotiated)
		return NULL;
	return &pw_data->cursor;
}

void pipewire_set_cursor_visible(pipewire_data *pw_data, bool cursor_visible)
{
	pw_data->cursor.visible = cursor_visible;
}
