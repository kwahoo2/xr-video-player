#include "mpv.hpp"
#include <mpv/client.h>
#include <mpv/render_gl.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <EGL/egl.h>


static const int ctx_attribs[] = {
#ifdef _DEBUG
	EGL_CONTEXT_OPENGL_DEBUG,
	EGL_TRUE,
#endif
	EGL_CONTEXT_OPENGL_PROFILE_MASK,
	EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT,
	EGL_CONTEXT_MAJOR_VERSION,
	3,
	EGL_CONTEXT_MINOR_VERSION,
	3,
	EGL_NONE,
};

static const EGLint ctx_config_attribs[] = {
	EGL_STENCIL_SIZE,
	0,
	EGL_DEPTH_SIZE,
	0,
	EGL_BUFFER_SIZE,
	32,
	EGL_ALPHA_SIZE,
	8,
	EGL_RENDERABLE_TYPE,
	EGL_OPENGL_BIT,
	EGL_SURFACE_TYPE,
	EGL_WINDOW_BIT | EGL_PBUFFER_BIT,
	EGL_NONE,
};

static bool exec_program_daemonized(const char **args) {
	/* 1 argument */
	if(args[0] == nullptr)
		return false;

	pid_t pid = vfork();
	if(pid == -1) {
		perror("Failed to vfork");
		return false;
	} else if(pid == 0) { /* child */
		setsid();
		signal(SIGHUP, SIG_IGN);

		// Daemonize child to make the parent the init process which will reap the zombie child
		pid_t second_child = vfork();
		if(second_child == 0) { // child
			execvp(args[0], (char* const*)args);
			perror("execvp");
			_exit(127);
		} else if(second_child != -1) {
			// TODO:
			_exit(0);
		}
	} else { /* parent */
		waitpid(pid, nullptr, 0); 
	}

	return true;
}

static void show_notification(const char *title, const char *msg, const char *urgency) {
	const char *args[] = { "notify-send", "-a", "vr-video-player", "-t", "10000", "-u", urgency, "--", title, msg, NULL };
	exec_program_daemonized(args);
}

static void* get_proc_address_mpv(void*, const char *name) {
	return (void*) eglGetProcAddress(name);
}

static void on_mpv_events(void *ctx) {
	Mpv *mpv = (Mpv*)ctx;
	SDL_Event event;
	event.type = mpv->wakeup_on_mpv_events;
	SDL_PushEvent(&event);
}

static void on_mpv_render_update(void *ctx) {
	Mpv *mpv = (Mpv*)ctx;
	SDL_Event event;
	event.type = mpv->wakeup_on_mpv_render_update;
	SDL_PushEvent(&event);
}

Mpv::~Mpv() {
	destroy();
}

bool Mpv::init(struct gl_egl_data *egl_data) {
	this->egl_data.mutex = egl_data->mutex;
	this->egl_data.mutex_counter = egl_data->mutex_counter;
	gl_egl_context_create(&this->egl_data, ctx_config_attribs, ctx_attribs);
	return true;
}

bool Mpv::create(bool use_system_mpv_config) {
	if(created)
		return false;

	mpv = mpv_create();
	if(!mpv) {
		fprintf(stderr, "Error: mpv_create failed\n");
		return false;
	}

	if(use_system_mpv_config) {
		mpv_set_option_string(mpv, "config", "yes");
		mpv_set_option_string(mpv, "load-scripts", "yes");
	}

	if(mpv_initialize(mpv) < 0) {
		fprintf(stderr, "Error: mpv_initialize failed\n");
		mpv_destroy(mpv);
		mpv = nullptr;
		return false;
	}

	// mpv_request_log_messages(mpv, "debug");
	gl_egl_device_enter_context(&egl_data);

	mpv_opengl_init_params gl_init_params;
	memset(&gl_init_params, 0, sizeof(gl_init_params));
	gl_init_params.get_proc_address = get_proc_address_mpv;

	int advanced_control = 1;

	mpv_render_param params[] = {
		{ MPV_RENDER_PARAM_API_TYPE, (void*) MPV_RENDER_API_TYPE_OPENGL },
		{ MPV_RENDER_PARAM_ADVANCED_CONTROL, &advanced_control },
		{ MPV_RENDER_PARAM_OPENGL_INIT_PARAMS, &gl_init_params },
		//{ MPV_RENDER_PARAM_BLOCK_FOR_TARGET_TIME, 0 }, // TODO: Manually sync after this
		{ MPV_RENDER_PARAM_INVALID, 0 }
	};

	mpv_set_option_string(mpv, "vd-lavc-dr", "yes");
	mpv_set_option_string(mpv, "vo", "libmpv");
	mpv_set_option_string(mpv, "gpu-api", "opengl");
	mpv_set_option_string(mpv, "hwdec", "auto");
	// mpv_set_option_string(mpv, "profile", "low-latency");
	// mpv_set_option_string(mpv, "terminal", "");

	// mpv_command_string(mpv, "script-binding stats/display-stats-toggle");
	int ret = mpv_render_context_create(&mpv_gl, mpv, params);
	if(ret < 0) {
		fprintf(stderr, "Error: mpv_render_context_create failed with: %s (%d)\n", mpv_error_string(ret), ret);
		mpv_destroy(mpv);
		mpv = nullptr;
		mpv_gl = nullptr;
		return false;
	}

	wakeup_on_mpv_render_update = SDL_RegisterEvents(1);
	wakeup_on_mpv_events = SDL_RegisterEvents(1);
	if(wakeup_on_mpv_render_update == (uint32_t)-1 || wakeup_on_mpv_events == (uint32_t)-1) {
		fprintf(stderr, "Error: SDL_RegisterEvents failed\n");
		// TODO: Remove registered events?
		wakeup_on_mpv_render_update = -1;
		wakeup_on_mpv_events = -1;
		mpv_render_context_free(mpv_gl);
		mpv_destroy(mpv);
		mpv = nullptr;
		mpv_gl = nullptr;
		return false;
	}

	mpv_set_wakeup_callback(mpv, on_mpv_events, this);
	mpv_render_context_set_update_callback(mpv_gl, on_mpv_render_update, this);

	gl_egl_device_leave_context(&egl_data);
	created = true;
	return true;
}

bool Mpv::destroy() {
	if(!created)
		return true;
	
	if(mpv_gl)
		mpv_render_context_free(mpv_gl);
	if(mpv)
		mpv_destroy(mpv);
	
	destroy_frame_buffer();
	gl_egl_context_destroy(&egl_data);

	created = false;
	return true;
}

bool Mpv::load_file(const char *path) {
	if(!created)
		return false;

	const char *cmd[] = { "loadfile", path, nullptr };
	mpv_command_async(mpv, 0, cmd);
	return true;
}

void Mpv::on_event(SDL_Event &event, bool *quit, int *error) {
	if(quit) *quit = false;
	if(error) *error = 0;
	if(!created)
		return;

	if(event.type == wakeup_on_mpv_render_update) {
		uint64_t flags = mpv_render_context_update(mpv_gl);
		if(flags & MPV_RENDER_UPDATE_FRAME) {
			set_render_update();
		}
	}

	if(event.type == wakeup_on_mpv_events) {
		while(true) {
			mpv_event *mp_event = mpv_wait_event(mpv, 0);
			if(mp_event->event_id == MPV_EVENT_NONE)
				break;
			
			if(mp_event->event_id == MPV_EVENT_LOG_MESSAGE) {
				mpv_event_log_message *msg = (mpv_event_log_message*)mp_event->data;
				//printf("log: %s", msg->text);
			} else {
				//printf("mpv event: %s\n", mpv_event_name(mp_event->event_id));
			}

			if(mp_event->event_id == MPV_EVENT_END_FILE) {
				mpv_event_end_file *msg = (mpv_event_end_file*)mp_event->data;
				if(msg->reason == MPV_END_FILE_REASON_ERROR) {
					show_notification("xr-video-player mpv video error", mpv_error_string(msg->error), "critical");
					running = false;
					if(quit) {
						*quit = true;
						*error = -1;
					}
				}
				if(msg->reason == MPV_END_FILE_REASON_EOF) {
					show_notification("xr-video-player", "the video ended", "low");
					/*
					running = false;
					if(quit)
						*quit = true;
					*/
				}
			}

			if(mp_event->event_id == MPV_EVENT_VIDEO_RECONFIG) {
				mpv_get_property(mpv, "width", MPV_FORMAT_INT64, &video_width);
				mpv_get_property(mpv, "height", MPV_FORMAT_INT64, &video_height);
				printf("[MPV] New size: %ld %ld\n", video_width, video_height);
				fflush(stdout);
				video_loaded = true;
				loaded_in_thread = false;
			}
		}
	}
}

void Mpv::seek(double seconds) {
	if(!created)
		return;

	char seconds_str[128];
	snprintf(seconds_str, sizeof(seconds_str), "%f", seconds);

	const char *cmd[] = { "seek", seconds_str, nullptr };
	mpv_command_async(mpv, 0, cmd);
}

void Mpv::toggle_pause() {
	if(!created)
		return;

	paused = !paused;
	int pause_value = paused ? 1 : 0;
	mpv_set_property_async(mpv, 0, "pause", MPV_FORMAT_FLAG, &pause_value);
}

void Mpv::draw(unsigned int framebuffer_id, int width, int height) {
	if(!created)
		return;

	mpv_opengl_fbo fbo;
	memset(&fbo, 0, sizeof(fbo));
	fbo.fbo = framebuffer_id;
	fbo.w = width;
	fbo.h = height;

	int flip_y = 0;
    int shit = 1;

	mpv_render_param params[] = {
		{ MPV_RENDER_PARAM_OPENGL_FBO, &fbo },
		{ MPV_RENDER_PARAM_FLIP_Y, &flip_y },
		//{ MPV_RENDER_PARAM_SKIP_RENDERING, &shit },
		{ MPV_RENDER_PARAM_INVALID, 0 }
	};

	int res = mpv_render_context_render(mpv_gl, params);
	//fprintf(stderr, "draw mpv: %d\n", res);
}

bool Mpv::take_render_update() {
	std::lock_guard<std::mutex> lock(render_update_mutex);
	bool should_update = render_update;
	render_update = false;
	return should_update;
}

void Mpv::set_render_update() {
	std::lock_guard<std::mutex> lock(render_update_mutex);
	render_update = true;
}

bool Mpv::create_frame_buffer() {
	if (texture_id_render)
		destroy_frame_buffer();
	glGenFramebuffers(1, &framebuffer_id_render);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_id_render);

	glGenTextures(1, &texture_id_render);
	glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, texture_id_render);
	glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 4, GL_RGBA8, video_width, video_height, true);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, texture_id_render, 0);

	glGenFramebuffers(1, &framebuffer_id_resolved );
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_id_resolved);

	glGenTextures(1, &texture_id_resolved);
	glBindTexture(GL_TEXTURE_2D, texture_id_resolved);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, video_width, video_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture_id_resolved, 0);

	// check FBO status
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
		return false;
	}

	glBindFramebuffer( GL_FRAMEBUFFER, 0 );

	return true;
}

void Mpv::destroy_frame_buffer() {
	if (!texture_id_render)
		return;
	glDeleteTextures( 1, &texture_id_render);
	glDeleteFramebuffers( 1, &framebuffer_id_render);
	glDeleteTextures( 1, &texture_id_resolved);
	glDeleteFramebuffers( 1, &framebuffer_id_resolved);
}

void Mpv::set_running(bool value) {
	running = value;
}

void Mpv::run(bool use_system_mpv_config, const char *path, GLuint *vao, GLuint *program_id) {
	if(!create(use_system_mpv_config))
		return;

	load_file(path);

	while(running) {
		if(video_loaded) {
			if(!loaded_in_thread) {
				loaded_in_thread = true;
				gl_egl_device_enter_context(&egl_data);
				create_frame_buffer();
				gl_egl_device_leave_context(&egl_data);
			}
			if(take_render_update()) {
				gl_egl_device_enter_context(&egl_data);
#if 1
				glBindFramebuffer( GL_FRAMEBUFFER, framebuffer_id_render );
				glViewport(0, 0, video_width, video_height);
				glDisable(GL_DEPTH_TEST);

				glBindVertexArray(*vao);
				glUseProgram(*program_id);

				draw(framebuffer_id_render, video_width, video_height);

				glBindVertexArray(0);
				glUseProgram(0);
				glBindFramebuffer(GL_FRAMEBUFFER, 0);
				
				glDisable(GL_MULTISAMPLE);

				glBindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer_id_render);
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer_id_resolved);
				
				glBlitFramebuffer(
					0, 0, video_width, video_height, 
					0, 0, video_width, video_height, 
					GL_COLOR_BUFFER_BIT, GL_LINEAR
				);

				glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

				glEnable(GL_MULTISAMPLE);
#else
				draw(framebuffer_id_resolved, video_width, video_height);
#endif
				gl_egl_device_leave_context(&egl_data);
			}
		} else {
			usleep(1000);
		}
	}
}

