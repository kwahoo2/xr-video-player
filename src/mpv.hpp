#pragma once

#include <stdint.h>
#include <SDL.h>

#include <thread>
#include <mutex>
#include <GL/glew.h>
#include "gl-egl-common.h"

typedef struct mpv_handle mpv_handle;
typedef struct mpv_render_context mpv_render_context;

class Mpv {
public:
	Mpv() = default;
	~Mpv();

	bool init(struct gl_egl_data *egl_data);
	bool create(bool use_system_mpv_config);
	bool destroy();

	bool load_file(const char *path);
	// |width| and |ħeight| are set to 0 unless there is an event to reconfigure video size
	void on_event(SDL_Event &event, bool *quit, int *error);
	void seek(double seconds);
	void toggle_pause();
	void draw(unsigned int framebuffer_id, int width, int height);
	bool take_render_update();
	void set_render_update();
	void set_running(bool value);
	void run(bool use_system_mpv_config, const char *path, GLuint *vao, GLuint *program_id);

	bool created = false;
	uint32_t wakeup_on_mpv_render_update = -1;
	uint32_t wakeup_on_mpv_events = -1;

	mpv_handle *mpv = nullptr;
	mpv_render_context *mpv_gl = nullptr;
	bool paused = false;

	int64_t video_width = 0;
	int64_t video_height = 0;
	bool video_loaded = false;
	bool loaded_in_thread = false;
	GLuint texture_id_resolved;
	
private:
	bool create_frame_buffer();
	void destroy_frame_buffer();
	
	struct gl_egl_data egl_data;
	GLuint texture_id_render;
	GLuint framebuffer_id_render;
	GLuint framebuffer_id_resolved;
	
	std::mutex render_update_mutex;
	bool render_update = false;
	bool running = true;
};
