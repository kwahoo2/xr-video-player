/******************************************************************************
    Copyright (C) 2020 by Georges Basile Stavracas Neto <georges.stavracas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "gl-egl-common.h"

#include <EGL/egl.h>
#include <EGL/eglext.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#if defined(__linux__)

#include <linux/types.h>
#include <asm/ioctl.h>
typedef unsigned int drm_handle_t;

#else

#include <stdint.h>
#include <sys/ioccom.h>
#include <sys/types.h>
typedef int8_t __s8;
typedef uint8_t __u8;
typedef int16_t __s16;
typedef uint16_t __u16;
typedef int32_t __s32;
typedef uint32_t __u32;
typedef int64_t __s64;
typedef uint64_t __u64;
typedef size_t __kernel_size_t;
typedef unsigned long drm_handle_t;

#endif

typedef EGLBoolean (*eglQueryDmaBufFormatsEXT_func)(EGLDisplay dpy,
                                                    EGLint max_formats,
                                                    EGLint* formats,
                                                    EGLint* num_formats);
eglQueryDmaBufFormatsEXT_func eglQueryDmaBufFormatsEXT = NULL;
typedef EGLBoolean (*eglQueryDmaBufModifiersEXT_func)(EGLDisplay dpy,
                                                      EGLint format,
                                                      EGLint max_modifiers,
                                                      EGLuint64KHR* modifiers,
                                                      EGLBoolean* external_only,
                                                      EGLint* num_modifiers);
eglQueryDmaBufModifiersEXT_func eglQueryDmaBufModifiersEXT = NULL;

PFNGLEGLIMAGETARGETTEXTURE2DOESPROC glEGLImageTargetTexture2DOES = NULL;

/*
static bool find_gl_extension(const char *extension)
{
	GLint n, i;

	glGetIntegerv(GL_NUM_EXTENSIONS, &n);
	for (i = 0; i < n; i++) {
        glGetString
		const char *e = (char *)glGetStringi(GL_EXTENSIONS, i);
		if (extension && strcmp(e, extension) == 0)
			return true;
	}
	return false;
}
*/

static bool init_egl_extensions(void)
{
	static bool initialized = false;

	if (!initialized) {
		initialized = true;
		if (!GL_OES_EGL_image) {
			printf("[ERROR] No GL_OES_EGL_image\n");
			return false;
		}
		if (!EGL_EXT_image_dma_buf_import) {
			printf("[ERROR] No EGL_EXT_image_dma_buf_import\n");
			return false;
		}
		if (!EGL_EXT_image_dma_buf_import_modifiers) {
			printf("[ERROR] No EGL_EXT_image_dma_buf_import_modifiers\n");
			return false;
		}
		

		glEGLImageTargetTexture2DOES = (PFNGLEGLIMAGETARGETTEXTURE2DOESPROC) eglGetProcAddress("glEGLImageTargetTexture2DOES");
		eglQueryDmaBufFormatsEXT = (eglQueryDmaBufFormatsEXT_func) eglGetProcAddress("eglQueryDmaBufFormatsEXT");
		eglQueryDmaBufModifiersEXT = (eglQueryDmaBufModifiersEXT_func) eglGetProcAddress("eglQueryDmaBufModifiersEXT");
	}

	if (!glEGLImageTargetTexture2DOES || !eglQueryDmaBufFormatsEXT || !eglQueryDmaBufModifiersEXT)
		return false;

	return true;
}

static EGLImageKHR
create_dmabuf_egl_image(EGLDisplay egl_display, unsigned int width,
			unsigned int height, uint32_t drm_format,
			uint32_t n_planes, const int *fds,
			const uint32_t *strides, const uint32_t *offsets,
			const uint64_t *modifiers)
{
	EGLAttrib attribs[47];
	int atti = 0;

	/* This requires the Mesa commit in
	 * Mesa 10.3 (08264e5dad4df448e7718e782ad9077902089a07) or
	 * Mesa 10.2.7 (55d28925e6109a4afd61f109e845a8a51bd17652).
	 * Otherwise Mesa closes the fd behind our back and re-importing
	 * will fail.
	 * https://bugs.freedesktop.org/show_bug.cgi?id=76188
	 * */

	attribs[atti++] = EGL_WIDTH;
	attribs[atti++] = width;
	attribs[atti++] = EGL_HEIGHT;
	attribs[atti++] = height;
	attribs[atti++] = EGL_LINUX_DRM_FOURCC_EXT;
	attribs[atti++] = drm_format;

	if (n_planes > 0) {
		attribs[atti++] = EGL_DMA_BUF_PLANE0_FD_EXT;
		attribs[atti++] = fds[0];
		attribs[atti++] = EGL_DMA_BUF_PLANE0_OFFSET_EXT;
		attribs[atti++] = offsets[0];
		attribs[atti++] = EGL_DMA_BUF_PLANE0_PITCH_EXT;
		attribs[atti++] = strides[0];
		if (modifiers) {
			attribs[atti++] = EGL_DMA_BUF_PLANE0_MODIFIER_LO_EXT;
			attribs[atti++] = modifiers[0] & 0xFFFFFFFF;
			attribs[atti++] = EGL_DMA_BUF_PLANE0_MODIFIER_HI_EXT;
			attribs[atti++] = modifiers[0] >> 32;
		}
	}

	if (n_planes > 1) {
		attribs[atti++] = EGL_DMA_BUF_PLANE1_FD_EXT;
		attribs[atti++] = fds[1];
		attribs[atti++] = EGL_DMA_BUF_PLANE1_OFFSET_EXT;
		attribs[atti++] = offsets[1];
		attribs[atti++] = EGL_DMA_BUF_PLANE1_PITCH_EXT;
		attribs[atti++] = strides[1];
		if (modifiers) {
			attribs[atti++] = EGL_DMA_BUF_PLANE1_MODIFIER_LO_EXT;
			attribs[atti++] = modifiers[1] & 0xFFFFFFFF;
			attribs[atti++] = EGL_DMA_BUF_PLANE1_MODIFIER_HI_EXT;
			attribs[atti++] = modifiers[1] >> 32;
		}
	}

	if (n_planes > 2) {
		attribs[atti++] = EGL_DMA_BUF_PLANE2_FD_EXT;
		attribs[atti++] = fds[2];
		attribs[atti++] = EGL_DMA_BUF_PLANE2_OFFSET_EXT;
		attribs[atti++] = offsets[2];
		attribs[atti++] = EGL_DMA_BUF_PLANE2_PITCH_EXT;
		attribs[atti++] = strides[2];
		if (modifiers) {
			attribs[atti++] = EGL_DMA_BUF_PLANE2_MODIFIER_LO_EXT;
			attribs[atti++] = modifiers[2] & 0xFFFFFFFF;
			attribs[atti++] = EGL_DMA_BUF_PLANE2_MODIFIER_HI_EXT;
			attribs[atti++] = modifiers[2] >> 32;
		}
	}

	if (n_planes > 3) {
		attribs[atti++] = EGL_DMA_BUF_PLANE3_FD_EXT;
		attribs[atti++] = fds[3];
		attribs[atti++] = EGL_DMA_BUF_PLANE3_OFFSET_EXT;
		attribs[atti++] = offsets[3];
		attribs[atti++] = EGL_DMA_BUF_PLANE3_PITCH_EXT;
		attribs[atti++] = strides[3];
		if (modifiers) {
			attribs[atti++] = EGL_DMA_BUF_PLANE3_MODIFIER_LO_EXT;
			attribs[atti++] = modifiers[3] & 0xFFFFFFFF;
			attribs[atti++] = EGL_DMA_BUF_PLANE3_MODIFIER_HI_EXT;
			attribs[atti++] = modifiers[3] >> 32;
		}
	}

	attribs[atti++] = EGL_NONE;

	return eglCreateImage(egl_display, EGL_NO_CONTEXT,
			      EGL_LINUX_DMA_BUF_EXT, 0, attribs);
}

GLuint gl_egl_create_texture_from_eglimage(
	EGLDisplay egl_display, uint32_t width, uint32_t height,
	gs_color_format color_format, EGLint target, EGLImage image, GLuint texture_reuse)
{
	UNUSED_PARAMETER(egl_display);
	UNUSED_PARAMETER(target);

	GLuint texture = 0;
	if (texture_reuse != 0) {
		texture = texture_reuse;
	} else {
		glGenTextures(1, &texture);
		if (texture == 0) {
			printf("[ERROR] Failed get new texture\n");
			return 0;
		}
	}

	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glEGLImageTargetTexture2DOES(GL_TEXTURE_2D, image);
	// TODO add check if it did what it should!

	glBindTexture(GL_TEXTURE_2D, 0);

	return texture;
}

GLuint gl_egl_create_dmabuf_image(EGLDisplay egl_display, unsigned int width,
			   unsigned int height, uint32_t drm_format,
			   gs_color_format color_format, uint32_t n_planes,
			   const int *fds, const uint32_t *strides,
			   const uint32_t *offsets, const uint64_t *modifiers, GLuint texture_reuse)
{
	GLuint texture;
	EGLImage egl_image;
	
	if (!init_egl_extensions()) {
		printf("[ERROR] Cannot init extensions: %s\n", gl_egl_error_to_string(eglGetError()));
		return false;
	}

	egl_image = create_dmabuf_egl_image(egl_display, width, height,
					    drm_format, n_planes, fds, strides,
					    offsets, modifiers);
	if (egl_image == EGL_NO_IMAGE) {
		printf("[ERROR] Cannot create EGLImage: %s\n",
		     gl_egl_error_to_string(eglGetError()));
		return 0;
	}

	texture = gl_egl_create_texture_from_eglimage(egl_display, width,
						      height, color_format,
						      GL_TEXTURE_2D, egl_image, texture_reuse);
	if (texture)
		eglDestroyImage(egl_display, egl_image);

	return texture;
}

GLuint
gl_egl_create_texture_from_pixmap(EGLDisplay egl_display, uint32_t width,
				  uint32_t height,
				  gs_color_format color_format,
				  EGLint target, EGLClientBuffer pixmap)
{
	if (!init_egl_extensions()) {
		printf("[ERROR] Cannot init extensions: %s\n", gl_egl_error_to_string(eglGetError()));
		return false;
	}

	const EGLAttrib pixmap_attrs[] = {
		EGL_IMAGE_PRESERVED_KHR,
		EGL_TRUE,
		EGL_NONE,
	};

	EGLImage image = eglCreateImage(egl_display, EGL_NO_CONTEXT,
					EGL_NATIVE_PIXMAP_KHR, pixmap,
					pixmap_attrs);
	if (image == EGL_NO_IMAGE) {
		printf("[DEBUG] Cannot create EGLImage: %s\n",
		     gl_egl_error_to_string(eglGetError()));
		return 0;
	}

	GLuint texture = gl_egl_create_texture_from_eglimage(
		egl_display, width, height, color_format, target, image, 0);
	eglDestroyImage(egl_display, image);

	return texture;
}

static inline bool is_implicit_dmabuf_modifiers_supported(void)
{
	return EGL_EXT_image_dma_buf_import > 0;
}

static inline bool query_dmabuf_formats(EGLDisplay egl_display,
					EGLint **formats, EGLint *num_formats)
{
	EGLint max_formats = 0;
	EGLint *format_list = NULL;
	if (!init_egl_extensions()) {
		printf("[ERROR] Cannot init extensions: %s\n", gl_egl_error_to_string(eglGetError()));
		return false;
	}

	if (!eglQueryDmaBufFormatsEXT(egl_display, 0, NULL,
					   &max_formats)) {
		printf("[ERROR] Cannot query the number of formats: %s\n",
		     gl_egl_error_to_string(eglGetError()));
		return false;
	}

	format_list = malloc(max_formats * sizeof(EGLint));
	if (!format_list) {
		printf("[ERROR] Unable to allocate memory\n");
		return false;
	}

	if (!eglQueryDmaBufFormatsEXT(egl_display, max_formats,
					   format_list, &max_formats)) {
		printf("[ERROR] Cannot query a list of formats: %s\n",
		     gl_egl_error_to_string(eglGetError()));
		free(format_list);
		return false;
	}

	*formats = format_list;
	*num_formats = max_formats;
	return true;
}

bool gl_egl_query_dmabuf_capabilities(EGLDisplay egl_display,
				      enum gs_dmabuf_flags *dmabuf_flags,
				      uint32_t **formats, size_t *n_formats)
{
	bool ret = false;
	*n_formats = 0;
	if (!init_egl_extensions()) {
		printf("[ERROR] Cannot init extensions: %s\n", gl_egl_error_to_string(eglGetError()));
		return false;
	}

	if (is_implicit_dmabuf_modifiers_supported()) {
		*dmabuf_flags = GS_DMABUF_FLAG_IMPLICIT_MODIFIERS_SUPPORTED;
		ret = true;
	}

	if (!eglQueryDmaBufFormatsEXT) {
		printf("[ERROR] Unable to load eglQueryDmaBufFormatsEXT\n");
		return ret;
	}

	if (!query_dmabuf_formats(egl_display, (EGLint **)formats,
				  (EGLint *)n_formats)) {
		*n_formats = 0;
		*formats = NULL;
	}
	return ret;
}

static inline bool query_dmabuf_modifiers(EGLDisplay egl_display,
					  EGLint drm_format,
					  EGLuint64KHR **modifiers,
					  EGLuint64KHR *n_modifiers)
{
	EGLint max_modifiers;

	if (!eglQueryDmaBufModifiersEXT(egl_display, drm_format, 0, NULL,
					     NULL, &max_modifiers)) {
		printf("[ERROR] Cannot query the number of modifiers: %s\n",
		     gl_egl_error_to_string(eglGetError()));
		return false;
	}

	EGLuint64KHR *modifier_list =
		malloc(max_modifiers * sizeof(EGLuint64KHR));
	EGLBoolean *external_only = NULL;
	if (!modifier_list) {
		printf("[ERROR] Unable to allocate memory\n");
		return false;
	}

	if (!eglQueryDmaBufModifiersEXT(egl_display, drm_format,
					     max_modifiers, modifier_list,
					     external_only, &max_modifiers)) {
		printf("[ERROR] Cannot query a list of modifiers: %s\n",
		     gl_egl_error_to_string(eglGetError()));
		free(modifier_list);
		return false;
	}

	*modifiers = modifier_list;
	*n_modifiers = (EGLuint64KHR)max_modifiers;
	return true;
}

bool gl_egl_query_dmabuf_modifiers_for_format(EGLDisplay egl_display,
					      uint32_t drm_format,
					      uint64_t **modifiers,
					      size_t *n_modifiers)
{
	if (!eglQueryDmaBufModifiersEXT) {
		printf("[ERROR] Unable to load eglQueryDmaBufModifiersEXT\n");
		return false;
	}
	if (!query_dmabuf_modifiers(egl_display, drm_format, modifiers,
				    n_modifiers)) {
		*n_modifiers = 0;
		*modifiers = NULL;
		return false;
	}
	return true;
}

const char *gl_egl_error_to_string(EGLint error_number)
{
	switch (error_number) {
	case EGL_SUCCESS:
		return "The last function succeeded without error.";
		break;
	case EGL_NOT_INITIALIZED:
		return "EGL is not initialized, or could not be initialized, for the specified EGL display connection.";
		break;
	case EGL_BAD_ACCESS:
		return "EGL cannot access a requested resource (for example a context is bound in another thread).";
		break;
	case EGL_BAD_ALLOC:
		return "EGL failed to allocate resources for the requested operation.";
		break;
	case EGL_BAD_ATTRIBUTE:
		return "An unrecognized attribute or attribute value was passed in the attribute list.";
		break;
	case EGL_BAD_CONTEXT:
		return "An EGLContext argument does not name a valid EGL rendering context.";
		break;
	case EGL_BAD_CONFIG:
		return "An EGLConfig argument does not name a valid EGL frame buffer configuration.";
		break;
	case EGL_BAD_CURRENT_SURFACE:
		return "The current surface of the calling thread is a window, pixel buffer or pixmap that is no longer valid.";
		break;
	case EGL_BAD_DISPLAY:
		return "An EGLDisplay argument does not name a valid EGL display connection.";
		break;
	case EGL_BAD_SURFACE:
		return "An EGLSurface argument does not name a valid surface (window, pixel buffer or pixmap) configured for GL rendering.";
		break;
	case EGL_BAD_MATCH:
		return "Arguments are inconsistent (for example, a valid context requires buffers not supplied by a valid surface).";
		break;
	case EGL_BAD_PARAMETER:
		return "One or more argument values are invalid.";
		break;
	case EGL_BAD_NATIVE_PIXMAP:
		return "A NativePixmapType argument does not refer to a valid native pixmap.";
		break;
	case EGL_BAD_NATIVE_WINDOW:
		return "A NativeWindowType argument does not refer to a valid native window.";
		break;
	case EGL_CONTEXT_LOST:
		return "A power management event has occurred. The application must destroy all contexts and reinitialise OpenGL ES state and objects to continue rendering. ";
		break;
	default:
		return "Unknown error";
		break;
	}
}

GLuint gl_egl_create_texture_from_bitmap(
	EGLDisplay egl_display, uint32_t width, uint32_t height,
	gs_color_format color_format, EGLint target, const uint8_t **data, GLuint texture_reuse)
{
	UNUSED_PARAMETER(egl_display);
	UNUSED_PARAMETER(target);

	GLuint texture = 0;
	if (texture_reuse != 0) {
		texture = texture_reuse;
	} else {
		glGenTextures(1, &texture);
		if (texture == 0) {
			printf("[ERROR] Failed get new texture\n");
			return 0;
		}
	}

	glBindTexture(GL_TEXTURE_2D, texture);
	// printf("[INFO] color_format: %04lX\n", color_format);
	
	glTexImage2D(GL_TEXTURE_2D, 0, color_format, width, height, 0, color_format, GL_UNSIGNED_BYTE, data ? *data : NULL);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);


	glBindTexture(GL_TEXTURE_2D, 0);

	return texture;
}

bool gl_egl_context_create(struct gl_egl_data *data, const EGLint *ctx_config_attribs, const EGLint *ctx_attribs)
{
	int frame_buf_config_count = 0;
	EGLDisplay edisplay = EGL_NO_DISPLAY;
	EGLConfig config = NULL;
	EGLContext context = EGL_NO_CONTEXT;
	int egl_min = 0, egl_maj = 0;
	bool success = false;

	eglBindAPI(EGL_OPENGL_API);

	edisplay = eglGetCurrentDisplay();

	if (EGL_NO_DISPLAY == edisplay) {
		printf(
		     "[ERROR] Failed to get EGL display using eglGetDisplay\n");
		return false;
	}

	if (!eglInitialize(edisplay, &egl_maj, &egl_min)) {
		printf("[ERROR] Failed to initialize EGL: %s\n",
		     gl_egl_error_to_string(eglGetError()));
		return false;
	}

	if (!eglChooseConfig(edisplay, ctx_config_attribs, &config, 1,
			     &frame_buf_config_count)) {
		printf("[ERROR] Unable to find suitable EGL config: %s\n",
		     gl_egl_error_to_string(eglGetError()));
		goto error;
	}

	context =
		eglCreateContext(edisplay, config, eglGetCurrentContext(), ctx_attribs);
#ifdef _DEBUG
	if (EGL_NO_CONTEXT == context) {
		const EGLint error = eglGetError();
		if (error == EGL_BAD_ATTRIBUTE) {
			/* Sometimes creation fails because debug gl is not supported */
			printf(
			     "[ERROR] Unable to create EGL context with DEBUG attrib, trying without\n");
			context = eglCreateContext(edisplay, config,
						   EGL_NO_CONTEXT,
						   ctx_attribs + 2);
		} else {
			printf("[ERROR] Unable to create EGL context: %s\n",
			     gl_egl_error_to_string(eglGetError(error)));
			goto error;
		}
	}
#endif
	if (EGL_NO_CONTEXT == context) {
		printf("[ERROR] Unable to create EGL context: %s\n",
		     gl_egl_error_to_string(eglGetError()));
		goto error;
	}

	data->surface = EGL_NO_SURFACE;
	data->surface_draw = EGL_NO_SURFACE;

	data->display = edisplay;
	data->config = config;
	data->context = context;

	success = true;
	printf("[DEBUG] Created EGLDisplay %p\n", data->display);

error:
	if (!success) {
		if (EGL_NO_CONTEXT != context)
			eglDestroyContext(edisplay, context);
		eglTerminate(edisplay);
	}

	return success;
}

void gl_egl_context_destroy(struct gl_egl_data *data)
{
	if (data->context == 0)
		return;
	
	eglMakeCurrent(data->display, EGL_NO_SURFACE, EGL_NO_SURFACE,
		       EGL_NO_CONTEXT);
	eglDestroyContext(data->display, data->context);
}

bool gl_egl_device_enter_context(struct gl_egl_data *data) {
	pthread_mutex_lock(data->mutex);
	// printf("%ld Enter layer\n", *data->mutex_counter);
	(*data->mutex_counter)++;
	if (*data->mutex_counter > 1)
		return true;
	
	if (eglBindAPI(EGL_OPENGL_API) == EGL_FALSE) {
		printf("[ERROR] eglBindAPI EGL_OPENGL_API failed: %s\n", gl_egl_error_to_string(eglGetError()));
	}
	if (!eglMakeCurrent(data->display, (data->surface_draw != EGL_NO_SURFACE ? data->surface_draw: data->surface), data->surface, data->context)) {
		printf("[ERROR] Failed to make context current: %s\n", gl_egl_error_to_string(eglGetError()));
		return false;
	}

	if (data->surface != EGL_NO_SURFACE)
		glDrawBuffer(GL_BACK);

	return true;
}

bool gl_egl_device_leave_context(struct gl_egl_data *data) {
	(*data->mutex_counter)--;
	// printf("%ld Exiting layer\n", *data->mutex_counter);
	if (*data->mutex_counter <= 0) {
		if (eglBindAPI(EGL_OPENGL_API) == EGL_FALSE) {
			printf("[ERROR] eglBindAPI EGL_OPENGL_API failed: %s\n", gl_egl_error_to_string(eglGetError()));
		}
		if (!eglMakeCurrent(data->display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT)) {
			printf("[ERROR] Failed to make context not current: %s\n", gl_egl_error_to_string(eglGetError()));
			return false;
		}
	}
	pthread_mutex_unlock(data->mutex);
	return true;
}

