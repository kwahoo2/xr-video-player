#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <GL/glew.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>

#include <stdbool.h>
#include <pthread.h>

#define UNUSED_PARAMETER(param) (void)param

typedef int64_t gs_color_format;

enum gs_dmabuf_flags {
	GS_DMABUF_FLAG_NONE,
	GS_DMABUF_FLAG_IMPLICIT_MODIFIERS_SUPPORTED
};

struct gl_egl_data {
	pthread_mutex_t *mutex;
	int64_t *mutex_counter;
	EGLDisplay display;
	EGLConfig config;
	EGLContext context;
	EGLSurface surface; // read and draw if draw is not defined
	EGLSurface surface_draw;
};

const char *gl_egl_error_to_string(EGLint error_number);

GLuint
gl_egl_create_dmabuf_image(EGLDisplay egl_display, unsigned int width,
			   unsigned int height, uint32_t drm_format,
			   gs_color_format color_format, uint32_t n_planes,
			   const int *fds, const uint32_t *strides,
			   const uint32_t *offsets, const uint64_t *modifiers, GLuint texture_reuse);

bool gl_egl_query_dmabuf_capabilities(EGLDisplay egl_display,
				      enum gs_dmabuf_flags *dmabuf_flags,
				      uint32_t **drm_format, size_t *n_formats);

bool gl_egl_query_dmabuf_modifiers_for_format(EGLDisplay egl_display,
					      uint32_t drm_format,
					      uint64_t **modifiers,
					      size_t *n_modifiers);

GLuint
gl_egl_create_texture_from_pixmap(EGLDisplay egl_display, uint32_t width,
				  uint32_t height,
				  gs_color_format color_format,
				  EGLint target, EGLClientBuffer pixmap);

GLuint gl_egl_create_texture_from_bitmap(
	EGLDisplay egl_display, uint32_t width, uint32_t height,
	gs_color_format color_format, EGLint target, const uint8_t **data, GLuint texture_reuse);

bool gl_egl_context_create(struct gl_egl_data *data, const EGLint *ctx_config_attribs, const EGLint *ctx_attribs);
void gl_egl_context_destroy(struct gl_egl_data *data);

bool gl_egl_device_enter_context(struct gl_egl_data *data);
bool gl_egl_device_leave_context(struct gl_egl_data *data);

#ifdef __cplusplus
}
#endif
